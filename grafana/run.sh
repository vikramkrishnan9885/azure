#!/bin/sh

rgp="test-grafana"
name="grafana-cluster"
location="EastUS"
#version="1.10.5"
nodevmsize="Standard_D2s_v3"

az group create --name $rgp --location $location

# az aks create --name $name --resource-group $rgp --location $location --dns-name-prefix $name --generate-ssh-keys --node-count 2 --node-vm-size $nodevmsize
# To enable automatic HTTP application routing
az aks create --name $name --resource-group $rgp --location $location --dns-name-prefix $name --generate-ssh-keys --node-count 2 --node-vm-size $nodevmsize --enable-addons monitoring,http_application_routing #--kubernetes-version $version
az aks get-credentials --resource-group $rgp --name $name
kubectl cluster-info
kubectl get nodes

# To run the dashboard a ClusterRole has to be first added because 
# we enabled RBAC on the cluster.
kubectl create -f https://raw.githubusercontent.com/PlagueHO/Workshop-AKS/master/src/helm/dashboard-clusterrole.yaml

# To open the Cluster Dashboard:
az aks browse --resource-group $rgp --name $name

###########################################################################
#
# Configure Helm Tiller
#
###########################################################################

# Normally, Helm can just be enabled and installed using helm init. 
# However, this will install into the default Kubernetes namespace which 
# we can't enable role based access (RBAC) control on. 
# The Grafana Helm chart requires RBAC so we'll need to create a new 
# Kubernetes namespace called tiller-world to install it into.

# Initialize the Helm Tiller with role based access control enabled into 
# a the tiller-world namespace with:
kubectl create -f https://raw.githubusercontent.com/PlagueHO/Workshop-AKS/master/src/helm/cluster-rbac.yaml
#helm init --service-account tiller

# Install Grafana using Helm
# Next, we'll install an instance of Grafana into our cluster using Helm 
# and then scale up the replica set to run 2 replicas.
# We need to pass some configuration information to Helm to tell it how 
# to configure our Grafana service, such as the plugins to include.

# To install and run Grafana:
# helm install stable/grafana --set "service.type=LoadBalancer,persistence.enabled=true,persistence.size=10Gi,
# Note: If you've enabled your AKS cluster including HTTP application routing, 
# you can have Grafana automatically register a DNS name in the Azure DNS Zone that was created for your AKS cluster:
dnsName=$(az aks show -n $name -g $rgp --query 'addonProfiles.httpApplicationRouting.config.HTTPApplicationRoutingZoneName' -o tsv)
echo $dnsName
helm install stable/grafana --set "service.type=LoadBalancer,persistence.enabled=true,persistence.size=10Gi,persistence.accessModes[0]=ReadWriteOnce,plugins=grafana-azure-monitor-datasource\,grafana-kubernetes-app,ingress.enabled=true,ingress.annotations.kubernetes\.io/ingress\.class=addon-http-application-routing,ingress.hosts[0]=grafana.$dnsName"

# Set a variable name from the name of the Grafana service that was started by helm:
serviceName="grafana_service"

# To get IP Address of the Grafana server:
kubectl get service $serviceName -o jsonpath="{.status.loadBalancer.ingress[0].ip}"; echo

# To get admin account password Grafana server:
kubectl get secret $serviceName -o jsonpath="{.data.admin-password}" | base64 --decode ; echo


sleep 3m
az group delete -n $rgp
