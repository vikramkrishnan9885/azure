Import-Module Az

# CREATE RESOURCE GROUP
$rgParams = @{
    Name="myResourceGroup"
    Location="eastus"
}

$ResGrp = New-AzResourceGroup @rgParams

$namespaceName = 'myEHNamespace'
New-AzEventHubNamespace -ResourceGroupName $rgParams.Name -NamespaceName $namespaceName -Location $rgParams.Location

$eventHubName = 'myEH'
New-AzEventHub -ResourceGroupName $rgParams.Name -NamespaceName $namespaceName -EventHubName $eventHubName -MessageRetentionInDays 3

$subscriptionId = 'a4a8c613-6bb3-4a55-90cd-be8a39ef320e'
$rgName = $rgParams.Name
"az monitor log-profiles create --name `'default`' --location null --locations `'global`' `'eastus`' `'westus`' --categories `'Delete`' `'Write`' `'Action`'  --enabled false --days 0 --service-bus-rule-id `'/subscriptions/$subscriptionId/resourceGroups/$rgName/providers/Microsoft.EventHub/namespaces/$namespaceName/authorizationrules/RootManageSharedAccessKey`'" | Out-File 'eh.sh'
bash eh.sh