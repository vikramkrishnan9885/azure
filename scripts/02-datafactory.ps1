Import-Module Az

# CREATE RESOURCE GROUP
$rgParams = @{
 Name="myResourceGroup"
 Location="eastus"
}
Write-Output "********************************************************"
Write-Output "Creating Resource Group with name"
Write-Output $rgParams.Name
Write-Output "********************************************************"
$ResGrp = New-AzResourceGroup @rgParams

# CREATE A STORAGE ACCOUNT
# Use the following example to create a storage account 
# with locally redundant storage (LRS) and blob encryption (enabled by default).
$storageAccountNameGuid = [System.guid]::NewGuid().toString()
$storageAccountNameGuidDashRemoved = $storageAccountNameGuid.replace("-","")
$storageAccountName = $storageAccountNameGuidDashRemoved.SubString(0,18)
Write-Output "********************************************************"
Write-Output "Creating Storage Account with name"
Write-Output $storageAccountName
Write-Output "********************************************************"
$storageAccount = New-AzStorageAccount -ResourceGroupName $rgParams.Name -Name $storageAccountName -SkuName Standard_LRS -Location $rgParams.Location 
$ctx = $storageAccount.Context

# CREATE DEFINITION FILE FOR LINKED SERVICE
Write-Output "==============================================================="
Write-Output "STORAGE ACCOUNT NAME"
Write-Output $storageAccount.StorageAccountName
Write-Output "STORAGE ACCOUNT KEY"
$storageAccountKey = (Get-AzStorageAccountKey -ResourceGroupName $ResGrp.ResourceGroupName -Name $storageAccountName).Value[0]
Write-Output $storageAccountKey
Write-Output "==============================================================="

$text = "{
    `"name`": `"AzureStorageLinkedService`",
    `"properties`": {
        `"annotations`": [],
        `"type`": `"AzureBlobStorage`",
        `"typeProperties`": {
            `"connectionString`": `"DefaultEndpointsProtocol=https;AccountName=$storageAccountName;AccountKey=$storageAccountKey;EndpointSuffix=core.windows.net`"
        }
    }
}"
$text | Out-File 'AzureStorageLinkedService.json'
# Other options include
# $text | Set-Content 'file.txt'
# or
# $text > 'file.txt'
# Append to file:
# $text | Add-Content 'file.txt'
# or
# $text | Out-File 'file.txt' -Append
# or
# $text >> 'file.txt'

# CREATE A CONTAINER
# Blobs are always uploaded into a container. 
# You can organize groups of blobs like the way you organize your 
# files on your computer in folders.
# Set the permissions to blob to allow public access of the files.
$containerName = "adftutorial"
New-AzStorageContainer -Name $containerName -Context $ctx -Permission blob

# UPLOAD FILES
# To upload a file to a block blob, get a container reference, 
# then get a reference to the block blob in that container. 
# Once you have the blob reference, you can upload data to it 
# by using Set-AzStorageBlobContent.
 
# upload a file
Set-AzStorageBlobContent -File "/Users/vikram.krishnan/Desktop/azure/scripts/emp.txt" -Container $containerName -Blob "input/emp.txt" -Context $ctx 

# CREATE A DATA FACTORY
$dataFactoryNameGuid = [System.guid]::NewGuid().toString()
$dataFactoryNameGuidDashRemoved = $dataFactoryNameGuid.replace("-","")
$dataFactoryName = $dataFactoryNameGuidDashRemoved.SubString(0,18)
$DataFactory = Set-AzDataFactoryV2 -ResourceGroupName $ResGrp.ResourceGroupName -Location $ResGrp.Location -Name $dataFactoryName

# CREATE A LINKED SERVICE
Set-AzDataFactoryV2LinkedService -DataFactoryName $DataFactory.DataFactoryName -ResourceGroupName $ResGrp.ResourceGroupName -Name "AzureStorageLinkedService" -DefinitionFile "AzureStorageLinkedService.json"

# CREATE DATASETS
# Input dataset
Set-AzDataFactoryV2Dataset -DataFactoryName $DataFactory.DataFactoryName -ResourceGroupName $ResGrp.ResourceGroupName -Name "InputDataset" -DefinitionFile "InputDataset.json"

# Output Dataset
Set-AzDataFactoryV2Dataset -DataFactoryName $DataFactory.DataFactoryName -ResourceGroupName $ResGrp.ResourceGroupName -Name "OutputDataset" -DefinitionFile "OutputDataset.json"

# CREATE PIPELINE
$DFPipeLine = Set-AzDataFactoryV2Pipeline `
    -DataFactoryName $DataFactory.DataFactoryName `
    -ResourceGroupName $ResGrp.ResourceGroupName `
    -Name "Adfv2QuickStartPipeline" `
    -DefinitionFile "Adfv2QuickStartPipeline.json"

# CREATE PIPELINE RUN
$RunId = Invoke-AzDataFactoryV2Pipeline `
  -DataFactoryName $DataFactory.DataFactoryName `
  -ResourceGroupName $ResGrp.ResourceGroupName `
  -PipelineName $DFPipeLine.Name 

# BASIC MONITORING
# Continuously check the pipeline run status until it finishes copying the data.
while ($True) {
    $Run = Get-AzDataFactoryV2PipelineRun `
        -ResourceGroupName $ResGrp.ResourceGroupName `
        -DataFactoryName $DataFactory.DataFactoryName `
        -PipelineRunId $RunId

    if ($Run) {
        if ($run.Status -ne 'InProgress') {
            Write-Output ("Pipeline run finished. The status is: " +  $Run.Status)
            $Run
            break
        }
        Write-Output "Pipeline is running...status: InProgress"
    }

    Start-Sleep -Seconds 10
}
# retrieve copy activity run details
Write-Output "Activity run details:"
$Result = Get-AzDataFactoryV2ActivityRun -DataFactoryName $DataFactory.DataFactoryName -ResourceGroupName $ResGrp.ResourceGroupName -PipelineRunId $RunId -RunStartedAfter (Get-Date).AddMinutes(-30) -RunStartedBefore (Get-Date).AddMinutes(30)
Write-Output "Activity 'Output' section:"
$Result.Output -join "`r`n"

Write-Output "Activity 'Error' section:"
$Result.Error -join "`r`n"

# CLEAN UP
$job = Remove-AzResourceGroup -Name $rgParams.Name -Force -AsJob
# To wait until the deletion is complete
Wait-Job -Id $job.Id
