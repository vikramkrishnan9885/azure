#!/bin/bash

# the call to $RANDOM will ensure we get a unique name.
storageName=mystorageaccount$RANDOM
functionAppName=myappsvcpfunc$RANDOM

# Create a resource group
az group create \
  --name myResourceGroup \
  --location westeurope

# Create an azure storage account
az storage account create \
  --name $storageName \
  --location westeurope \
  --resource-group myResourceGroup \
  --sku Standard_LRS

# Create an App Service plan
az appservice plan create \
  --name myappserviceplan \
  --resource-group myResourceGroup \
  --location westeurope

# Create a Function App
az functionapp create \
  --name $functionAppName \
  --storage-account $storageName \
  --plan myappserviceplan \
  --resource-group myResourceGroup

# Publish
func azure functionapp publish $functionAppName