Last login: Tue Dec  3 17:45:10 on ttys000
ITSG003877-MAC:~ vikram.krishnan$ cd ~/Desktop/azure
ITSG003877-MAC:azure vikram.krishnan$ npm install -g purescript
/usr/local/bin/purs -> /usr/local/lib/node_modules/purescript/purs.bin

> purescript@0.13.5 postinstall /usr/local/lib/node_modules/purescript
> install-purescript --purs-ver=0.13.5

✔ Check if a prebuilt 0.13.5 binary is provided for darwin…
✔ Download the prebuilt PureScript binary (9s)
✔ Verify the prebuilt binary works correctly (118ms)…
✔ Save the downloaded binary to the npm cache directory

Installed to /usr/local/lib/node_modules/purescript/purs.bin 28.09 MB
Cached to /Users/vikram.krishnan/Library/Caches/purescript-npm-installer-nodejs/content-v2/sha512/bc/1b 28.09 MB

+ purescript@0.13.5
added 154 packages from 89 contributors in 15.443s
ITSG003877-MAC:azure vikram.krishnan$ npm install -g bower pulp
npm WARN deprecated bower@1.8.8: We don't recommend using Bower for new projects. Please consider Yarn and Webpack or Parcel. You can read how to migrate legacy project here: https://bower.io/blog/2017/how-to-migrate-away-from-bower/
/usr/local/bin/bower -> /usr/local/lib/node_modules/bower/bin/bower
/usr/local/bin/pulp -> /usr/local/lib/node_modules/pulp/index.js
+ bower@1.8.8
+ pulp@13.0.0
added 175 packages from 141 contributors in 10.424s
ITSG003877-MAC:azure vikram.krishnan$ pulp init --force
* Generating project skeleton in /Users/vikram.krishnan/Desktop/azure
bower not-cached    https://github.com/purescript/purescript-effect.git#*
bower resolve       https://github.com/purescript/purescript-effect.git#*
bower not-cached    https://github.com/purescript/purescript-console.git#*
bower resolve       https://github.com/purescript/purescript-console.git#*
bower not-cached    https://github.com/purescript/purescript-prelude.git#*
bower resolve       https://github.com/purescript/purescript-prelude.git#*
bower download      https://github.com/purescript/purescript-console/archive/v4.2.0.tar.gz
bower download      https://github.com/purescript/purescript-effect/archive/v2.0.1.tar.gz
bower download      https://github.com/purescript/purescript-prelude/archive/v4.1.1.tar.gz
bower extract       purescript-console#* archive.tar.gz
bower resolved      https://github.com/purescript/purescript-console.git#4.2.0
bower not-cached    https://github.com/purescript/purescript-effect.git#^2.0.0
bower resolve       https://github.com/purescript/purescript-effect.git#^2.0.0
bower not-cached    https://github.com/purescript/purescript-prelude.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-prelude.git#^4.0.0
bower download      https://github.com/purescript/purescript-prelude/archive/v4.1.1.tar.gz
bower download      https://github.com/purescript/purescript-effect/archive/v2.0.1.tar.gz
bower extract       purescript-effect#^2.0.0 archive.tar.gz
bower extract       purescript-effect#* archive.tar.gz
bower resolved      https://github.com/purescript/purescript-effect.git#2.0.1
bower resolved      https://github.com/purescript/purescript-effect.git#2.0.1
bower extract       purescript-prelude#* archive.tar.gz
bower resolved      https://github.com/purescript/purescript-prelude.git#4.1.1
bower extract       purescript-prelude#^4.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-prelude.git#4.1.1
bower install       purescript-console#4.2.0
bower install       purescript-effect#2.0.1
bower install       purescript-prelude#4.1.1

purescript-console#4.2.0 bower_components/purescript-console
├── purescript-effect#2.0.1
└── purescript-prelude#4.1.1

purescript-effect#2.0.1 bower_components/purescript-effect
└── purescript-prelude#4.1.1

purescript-prelude#4.1.1 bower_components/purescript-prelude
bower not-cached    https://github.com/purescript/purescript-psci-support.git#*
bower resolve       https://github.com/purescript/purescript-psci-support.git#*
bower download      https://github.com/purescript/purescript-psci-support/archive/v4.0.0.tar.gz
bower extract       purescript-psci-support#* archive.tar.gz
bower resolved      https://github.com/purescript/purescript-psci-support.git#4.0.0
bower install       purescript-psci-support#4.0.0

purescript-psci-support#4.0.0 bower_components/purescript-psci-support
├── purescript-console#4.2.0
├── purescript-effect#2.0.1
└── purescript-prelude#4.1.1
ITSG003877-MAC:azure vikram.krishnan$ pulp build
* Building project in /Users/vikram.krishnan/Desktop/azure
Compiling Type.Data.RowList
Compiling Data.NaturalTransformation
Compiling Record.Unsafe
Compiling Type.Data.Row
Compiling Data.Symbol
Compiling Data.Boolean
Compiling Control.Semigroupoid
Compiling Control.Category
Compiling Data.Show
Compiling Data.Unit
Compiling Data.Void
Compiling Data.Semiring
Compiling Data.HeytingAlgebra
Compiling Data.Semigroup
Compiling Data.Ring
Compiling Data.BooleanAlgebra
Compiling Data.Eq
Compiling Data.CommutativeRing
Compiling Data.Ordering
Compiling Data.EuclideanRing
Compiling Data.Ord.Unsafe
Compiling Data.Ord
Compiling Data.DivisionRing
Compiling Data.Field
Compiling Data.Monoid
Compiling Data.Bounded
Compiling Data.Function
Compiling Data.Functor
Compiling Control.Apply
Compiling Control.Applicative
Compiling Control.Bind
Compiling Control.Monad
Compiling Prelude
Compiling Data.Monoid.Endo
Compiling Data.Semigroup.First
Compiling Data.Monoid.Disj
Compiling Effect
Compiling Data.Semigroup.Last
Compiling Data.Monoid.Dual
Compiling Data.Monoid.Conj
Compiling Data.Monoid.Multiplicative
Compiling Data.Monoid.Additive
Compiling Effect.Class
Compiling Effect.Console
Compiling Effect.Unsafe
Compiling Effect.Uncurried
Compiling PSCI.Support
Compiling Main
Compiling Effect.Class.Console
* Build successful.
ITSG003877-MAC:azure vikram.krishnan$ bower install purescript-lists --save
bower not-cached    https://github.com/paf31/purescript-lists.git#*
bower resolve       https://github.com/paf31/purescript-lists.git#*
bower download      https://github.com/paf31/purescript-lists/archive/v5.4.1.tar.gz
bower extract       purescript-lists#* archive.tar.gz
bower resolved      https://github.com/paf31/purescript-lists.git#5.4.1
bower not-cached    https://github.com/purescript-contrib/purescript-bifunctors.git#^4.0.0
bower resolve       https://github.com/purescript-contrib/purescript-bifunctors.git#^4.0.0
bower not-cached    https://github.com/paf31/purescript-tailrec.git#^4.0.0
bower resolve       https://github.com/paf31/purescript-tailrec.git#^4.0.0
bower not-cached    https://github.com/purescript/purescript-tuples.git#^5.0.0
bower resolve       https://github.com/purescript/purescript-tuples.git#^5.0.0
bower not-cached    https://github.com/purescript/purescript-foldable-traversable.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-foldable-traversable.git#^4.0.0
bower not-cached    https://github.com/purescript/purescript-partial.git#^2.0.0
bower resolve       https://github.com/purescript/purescript-partial.git#^2.0.0
bower not-cached    https://github.com/purescript/purescript-control.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-control.git#^4.0.0
bower not-cached    https://github.com/purescript/purescript-lazy.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-lazy.git#^4.0.0
bower not-cached    https://github.com/paf31/purescript-unfoldable.git#^4.0.0
bower resolve       https://github.com/paf31/purescript-unfoldable.git#^4.0.0
bower not-cached    https://github.com/garyb/purescript-newtype.git#^3.0.0
bower resolve       https://github.com/garyb/purescript-newtype.git#^3.0.0
bower not-cached    https://github.com/purescript/purescript-maybe.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-maybe.git#^4.0.0
bower not-cached    https://github.com/paf31/purescript-nonempty.git#^5.0.0
bower resolve       https://github.com/paf31/purescript-nonempty.git#^5.0.0
bower download      https://github.com/purescript-contrib/purescript-bifunctors/archive/v4.0.0.tar.gz
bower download      https://github.com/purescript/purescript-partial/archive/v2.0.1.tar.gz
bower download      https://github.com/paf31/purescript-tailrec/archive/v4.1.0.tar.gz
bower download      https://github.com/purescript/purescript-tuples/archive/v5.1.0.tar.gz
bower download      https://github.com/purescript/purescript-foldable-traversable/archive/v4.1.1.tar.gz
bower download      https://github.com/purescript/purescript-lazy/archive/v4.0.0.tar.gz
bower extract       purescript-partial#^2.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-partial.git#2.0.1
bower extract       purescript-tuples#^5.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-tuples.git#5.1.0
bower not-cached    https://github.com/purescript/purescript-type-equality.git#^3.0.0
bower resolve       https://github.com/purescript/purescript-type-equality.git#^3.0.0
bower not-cached    https://github.com/mankyKitty/purescript-distributive.git#^4.0.0
bower resolve       https://github.com/mankyKitty/purescript-distributive.git#^4.0.0
bower download      https://github.com/garyb/purescript-newtype/archive/v3.0.0.tar.gz
bower download      https://github.com/paf31/purescript-unfoldable/archive/v4.0.2.tar.gz
bower not-cached    https://github.com/purescript/purescript-invariant.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-invariant.git#^4.0.0
bower download      https://github.com/purescript/purescript-control/archive/v4.2.0.tar.gz
bower extract       purescript-foldable-traversable#^4.0.0 archive.tar.gz
bower extract       purescript-tailrec#^4.0.0 archive.tar.gz
bower resolved      https://github.com/paf31/purescript-tailrec.git#4.1.0
bower resolved      https://github.com/purescript/purescript-foldable-traversable.git#4.1.1
bower not-cached    https://github.com/purescript/purescript-identity.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-identity.git#^4.0.0
bower not-cached    https://github.com/purescript/purescript-either.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-either.git#^4.0.0
bower download      https://github.com/purescript/purescript-maybe/archive/v4.0.1.tar.gz
bower not-cached    https://github.com/purescript/purescript-refs.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-refs.git#^4.0.0
bower not-cached    https://github.com/purescript/purescript-orders.git#^4.0.0
bower resolve       https://github.com/purescript/purescript-orders.git#^4.0.0
bower download      https://github.com/paf31/purescript-nonempty/archive/v5.0.0.tar.gz
bower extract       purescript-lazy#^4.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-lazy.git#4.0.0
bower extract       purescript-bifunctors#^4.0.0 archive.tar.gz
bower extract       purescript-control#^4.0.0 archive.tar.gz
bower resolved      https://github.com/purescript-contrib/purescript-bifunctors.git#4.0.0
bower resolved      https://github.com/purescript/purescript-control.git#4.2.0
bower download      https://github.com/mankyKitty/purescript-distributive/archive/v4.0.0.tar.gz
bower download      https://github.com/purescript/purescript-type-equality/archive/v3.0.0.tar.gz
bower download      https://github.com/purescript/purescript-invariant/archive/v4.1.0.tar.gz
bower extract       purescript-unfoldable#^4.0.0 archive.tar.gz
bower resolved      https://github.com/paf31/purescript-unfoldable.git#4.0.2
bower extract       purescript-newtype#^3.0.0 archive.tar.gz
bower resolved      https://github.com/garyb/purescript-newtype.git#3.0.0
bower download      https://github.com/purescript/purescript-identity/archive/v4.1.0.tar.gz
bower download      https://github.com/purescript/purescript-either/archive/v4.1.1.tar.gz
bower extract       purescript-nonempty#^5.0.0 archive.tar.gz
bower resolved      https://github.com/paf31/purescript-nonempty.git#5.0.0
bower extract       purescript-type-equality#^3.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-type-equality.git#3.0.0
bower extract       purescript-invariant#^4.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-invariant.git#4.1.0
bower download      https://github.com/purescript/purescript-refs/archive/v4.1.0.tar.gz
bower download      https://github.com/purescript/purescript-orders/archive/v4.0.0.tar.gz
bower extract       purescript-refs#^4.0.0 archive.tar.gz
bower extract       purescript-orders#^4.0.0 archive.tar.gz
bower extract       purescript-maybe#^4.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-refs.git#4.1.0
bower resolved      https://github.com/purescript/purescript-maybe.git#4.0.1
bower resolved      https://github.com/purescript/purescript-orders.git#4.0.0
bower extract       purescript-identity#^4.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-identity.git#4.1.0
bower extract       purescript-distributive#^4.0.0 archive.tar.gz
bower resolved      https://github.com/mankyKitty/purescript-distributive.git#4.0.0
bower extract       purescript-either#^4.0.0 archive.tar.gz
bower resolved      https://github.com/purescript/purescript-either.git#4.1.1
bower install       purescript-lists#5.4.1
bower install       purescript-partial#2.0.1
bower install       purescript-tuples#5.1.0
bower install       purescript-tailrec#4.1.0
bower install       purescript-foldable-traversable#4.1.1
bower install       purescript-lazy#4.0.0
bower install       purescript-bifunctors#4.0.0
bower install       purescript-control#4.2.0
bower install       purescript-unfoldable#4.0.2
bower install       purescript-newtype#3.0.0
bower install       purescript-nonempty#5.0.0
bower install       purescript-type-equality#3.0.0
bower install       purescript-invariant#4.1.0
bower install       purescript-refs#4.1.0
bower install       purescript-maybe#4.0.1
bower install       purescript-orders#4.0.0
bower install       purescript-identity#4.1.0
bower install       purescript-distributive#4.0.0
bower install       purescript-either#4.1.1

purescript-lists#5.4.1 bower_components/purescript-lists
├── purescript-bifunctors#4.0.0
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-lazy#4.0.0
├── purescript-maybe#4.0.1
├── purescript-newtype#3.0.0
├── purescript-nonempty#5.0.0
├── purescript-partial#2.0.1
├── purescript-prelude#4.1.1
├── purescript-tailrec#4.1.0
├── purescript-tuples#5.1.0
└── purescript-unfoldable#4.0.2

purescript-partial#2.0.1 bower_components/purescript-partial

purescript-tuples#5.1.0 bower_components/purescript-tuples
├── purescript-bifunctors#4.0.0
├── purescript-control#4.2.0
├── purescript-distributive#4.0.0
├── purescript-foldable-traversable#4.1.1
├── purescript-invariant#4.1.0
├── purescript-maybe#4.0.1
├── purescript-newtype#3.0.0
├── purescript-prelude#4.1.1
└── purescript-type-equality#3.0.0

purescript-tailrec#4.1.0 bower_components/purescript-tailrec
├── purescript-bifunctors#4.0.0
├── purescript-effect#2.0.1
├── purescript-either#4.1.1
├── purescript-identity#4.1.0
├── purescript-maybe#4.0.1
├── purescript-partial#2.0.1
├── purescript-prelude#4.1.1
└── purescript-refs#4.1.0

purescript-foldable-traversable#4.1.1 bower_components/purescript-foldable-traversable
├── purescript-bifunctors#4.0.0
├── purescript-control#4.2.0
├── purescript-maybe#4.0.1
├── purescript-newtype#3.0.0
├── purescript-orders#4.0.0
└── purescript-prelude#4.1.1

purescript-lazy#4.0.0 bower_components/purescript-lazy
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-invariant#4.1.0
└── purescript-prelude#4.1.1

purescript-bifunctors#4.0.0 bower_components/purescript-bifunctors
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-control#4.2.0 bower_components/purescript-control
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-unfoldable#4.0.2 bower_components/purescript-unfoldable
├── purescript-foldable-traversable#4.1.1
├── purescript-maybe#4.0.1
├── purescript-partial#2.0.1
├── purescript-prelude#4.1.1
└── purescript-tuples#5.1.0

purescript-newtype#3.0.0 bower_components/purescript-newtype
└── purescript-prelude#4.1.1

purescript-nonempty#5.0.0 bower_components/purescript-nonempty
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-maybe#4.0.1
├── purescript-prelude#4.1.1
├── purescript-tuples#5.1.0
└── purescript-unfoldable#4.0.2

purescript-type-equality#3.0.0 bower_components/purescript-type-equality

purescript-invariant#4.1.0 bower_components/purescript-invariant
└── purescript-prelude#4.1.1

purescript-refs#4.1.0 bower_components/purescript-refs
├── purescript-effect#2.0.1
└── purescript-prelude#4.1.1

purescript-maybe#4.0.1 bower_components/purescript-maybe
├── purescript-control#4.2.0
├── purescript-invariant#4.1.0
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-orders#4.0.0 bower_components/purescript-orders
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-identity#4.1.0 bower_components/purescript-identity
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-invariant#4.1.0
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-distributive#4.0.0 bower_components/purescript-distributive
├── purescript-identity#4.1.0
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-either#4.1.1 bower_components/purescript-either
├── purescript-bifunctors#4.0.0
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-invariant#4.1.0
├── purescript-maybe#4.0.1
└── purescript-prelude#4.1.1
ITSG003877-MAC:azure vikram.krishnan$ ls
Get started with Azure PowerShell | Microsoft Docs.pdf
PowerShell for Programmers_ How to write a function the right way _ Scripting Blog.htm
PowerShell for Programmers_ How to write a function the right way _ Scripting Blog_files
Screen Shot 2019-12-03 at 1.46.26 PM.png
about_Splatting - PowerShell | Microsoft Docs.pdf
beginning-powershell.txt
bower.json
bower_components
more-pwsh.txt
output
run.sh
scripts
src
still-more-pwsh.txt
test
ITSG003877-MAC:azure vikram.krishnan$ cd scripts/03-azure-funcs/
ITSG003877-MAC:03-azure-funcs vikram.krishnan$ pulp init --force
* Generating project skeleton in /Users/vikram.krishnan/Desktop/azure/scripts/03-azure-funcs
bower cached        https://github.com/purescript/purescript-console.git#4.2.0
bower validate      4.2.0 against https://github.com/purescript/purescript-console.git#*
bower cached        https://github.com/purescript/purescript-effect.git#2.0.1
bower validate      2.0.1 against https://github.com/purescript/purescript-effect.git#*
bower cached        https://github.com/purescript/purescript-prelude.git#4.1.1
bower validate      4.1.1 against https://github.com/purescript/purescript-prelude.git#*
bower cached        https://github.com/purescript/purescript-effect.git#2.0.1
bower validate      2.0.1 against https://github.com/purescript/purescript-effect.git#^2.0.0
bower cached        https://github.com/purescript/purescript-prelude.git#4.1.1
bower validate      4.1.1 against https://github.com/purescript/purescript-prelude.git#^4.0.0
bower install       purescript-console#4.2.0
bower install       purescript-effect#2.0.1
bower install       purescript-prelude#4.1.1

purescript-console#4.2.0 bower_components/purescript-console
├── purescript-effect#2.0.1
└── purescript-prelude#4.1.1

purescript-effect#2.0.1 bower_components/purescript-effect
└── purescript-prelude#4.1.1

purescript-prelude#4.1.1 bower_components/purescript-prelude
bower cached        https://github.com/purescript/purescript-psci-support.git#4.0.0
bower validate      4.0.0 against https://github.com/purescript/purescript-psci-support.git#*
bower install       purescript-psci-support#4.0.0

purescript-psci-support#4.0.0 bower_components/purescript-psci-support
├── purescript-console#4.2.0
├── purescript-effect#2.0.1
└── purescript-prelude#4.1.1
ITSG003877-MAC:03-azure-funcs vikram.krishnan$ pulp build
* Building project in /Users/vikram.krishnan/Desktop/azure/scripts/03-azure-funcs
Compiling Type.Data.Row
Compiling Data.NaturalTransformation
Compiling Type.Data.RowList
Compiling Record.Unsafe
Compiling Data.Symbol
Compiling Data.Boolean
Compiling Control.Semigroupoid
Compiling Data.Show
Compiling Control.Category
Compiling Data.Void
Compiling Data.Unit
Compiling Data.Semiring
Compiling Data.HeytingAlgebra
Compiling Data.Semigroup
Compiling Data.Ring
Compiling Data.BooleanAlgebra
Compiling Data.Eq
Compiling Data.CommutativeRing
Compiling Data.EuclideanRing
Compiling Data.Ordering
Compiling Data.Ord.Unsafe
Compiling Data.Ord
Compiling Data.DivisionRing
Compiling Data.Field
Compiling Data.Bounded
Compiling Data.Monoid
Compiling Data.Function
Compiling Data.Functor
Compiling Control.Apply
Compiling Control.Applicative
Compiling Control.Bind
Compiling Control.Monad
Compiling Prelude
Compiling Data.Monoid.Multiplicative
Compiling Data.Monoid.Dual
Compiling Data.Semigroup.First
Compiling Data.Monoid.Disj
Compiling Data.Monoid.Conj
Compiling Data.Monoid.Endo
Compiling Data.Monoid.Additive
Compiling Data.Semigroup.Last
Compiling Effect
Compiling Effect.Unsafe
Compiling Effect.Class
Compiling Effect.Uncurried
Compiling Effect.Console
Compiling Main
Compiling PSCI.Support
Compiling Effect.Class.Console
* Build successful.
ITSG003877-MAC:03-azure-funcs vikram.krishnan$ bower install purescript-lists --save
bower cached        https://github.com/paf31/purescript-lists.git#5.4.1
bower validate      5.4.1 against https://github.com/paf31/purescript-lists.git#*
bower cached        https://github.com/purescript/purescript-foldable-traversable.git#4.1.1
bower validate      4.1.1 against https://github.com/purescript/purescript-foldable-traversable.git#^4.0.0
bower cached        https://github.com/purescript-contrib/purescript-bifunctors.git#4.0.0
bower validate      4.0.0 against https://github.com/purescript-contrib/purescript-bifunctors.git#^4.0.0
bower cached        https://github.com/purescript/purescript-lazy.git#4.0.0
bower validate      4.0.0 against https://github.com/purescript/purescript-lazy.git#^4.0.0
bower cached        https://github.com/paf31/purescript-nonempty.git#5.0.0
bower validate      5.0.0 against https://github.com/paf31/purescript-nonempty.git#^5.0.0
bower cached        https://github.com/purescript/purescript-maybe.git#4.0.1
bower validate      4.0.1 against https://github.com/purescript/purescript-maybe.git#^4.0.0
bower cached        https://github.com/purescript/purescript-control.git#4.2.0
bower validate      4.2.0 against https://github.com/purescript/purescript-control.git#^4.0.0
bower cached        https://github.com/paf31/purescript-tailrec.git#4.1.0
bower validate      4.1.0 against https://github.com/paf31/purescript-tailrec.git#^4.0.0
bower cached        https://github.com/garyb/purescript-newtype.git#3.0.0
bower validate      3.0.0 against https://github.com/garyb/purescript-newtype.git#^3.0.0
bower cached        https://github.com/purescript/purescript-tuples.git#5.1.0
bower validate      5.1.0 against https://github.com/purescript/purescript-tuples.git#^5.0.0
bower cached        https://github.com/paf31/purescript-unfoldable.git#4.0.2
bower validate      4.0.2 against https://github.com/paf31/purescript-unfoldable.git#^4.0.0
bower cached        https://github.com/purescript/purescript-partial.git#2.0.1
bower validate      2.0.1 against https://github.com/purescript/purescript-partial.git#^2.0.0
bower cached        https://github.com/purescript/purescript-invariant.git#4.1.0
bower validate      4.1.0 against https://github.com/purescript/purescript-invariant.git#^4.0.0
bower cached        https://github.com/purescript/purescript-orders.git#4.0.0
bower validate      4.0.0 against https://github.com/purescript/purescript-orders.git#^4.0.0
bower cached        https://github.com/purescript/purescript-either.git#4.1.1
bower validate      4.1.1 against https://github.com/purescript/purescript-either.git#^4.0.0
bower cached        https://github.com/purescript/purescript-identity.git#4.1.0
bower validate      4.1.0 against https://github.com/purescript/purescript-identity.git#^4.0.0
bower cached        https://github.com/purescript/purescript-refs.git#4.1.0
bower validate      4.1.0 against https://github.com/purescript/purescript-refs.git#^4.0.0
bower cached        https://github.com/purescript/purescript-type-equality.git#3.0.0
bower validate      3.0.0 against https://github.com/purescript/purescript-type-equality.git#^3.0.0
bower cached        https://github.com/mankyKitty/purescript-distributive.git#4.0.0
bower validate      4.0.0 against https://github.com/mankyKitty/purescript-distributive.git#^4.0.0
bower install       purescript-lists#5.4.1
bower install       purescript-lazy#4.0.0
bower install       purescript-bifunctors#4.0.0
bower install       purescript-nonempty#5.0.0
bower install       purescript-foldable-traversable#4.1.1
bower install       purescript-maybe#4.0.1
bower install       purescript-tailrec#4.1.0
bower install       purescript-newtype#3.0.0
bower install       purescript-tuples#5.1.0
bower install       purescript-control#4.2.0
bower install       purescript-unfoldable#4.0.2
bower install       purescript-partial#2.0.1
bower install       purescript-invariant#4.1.0
bower install       purescript-orders#4.0.0
bower install       purescript-either#4.1.1
bower install       purescript-identity#4.1.0
bower install       purescript-distributive#4.0.0
bower install       purescript-type-equality#3.0.0
bower install       purescript-refs#4.1.0

purescript-lists#5.4.1 bower_components/purescript-lists
├── purescript-bifunctors#4.0.0
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-lazy#4.0.0
├── purescript-maybe#4.0.1
├── purescript-newtype#3.0.0
├── purescript-nonempty#5.0.0
├── purescript-partial#2.0.1
├── purescript-prelude#4.1.1
├── purescript-tailrec#4.1.0
├── purescript-tuples#5.1.0
└── purescript-unfoldable#4.0.2

purescript-lazy#4.0.0 bower_components/purescript-lazy
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-invariant#4.1.0
└── purescript-prelude#4.1.1

purescript-bifunctors#4.0.0 bower_components/purescript-bifunctors
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-nonempty#5.0.0 bower_components/purescript-nonempty
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-maybe#4.0.1
├── purescript-prelude#4.1.1
├── purescript-tuples#5.1.0
└── purescript-unfoldable#4.0.2

purescript-foldable-traversable#4.1.1 bower_components/purescript-foldable-traversable
├── purescript-bifunctors#4.0.0
├── purescript-control#4.2.0
├── purescript-maybe#4.0.1
├── purescript-newtype#3.0.0
├── purescript-orders#4.0.0
└── purescript-prelude#4.1.1

purescript-maybe#4.0.1 bower_components/purescript-maybe
├── purescript-control#4.2.0
├── purescript-invariant#4.1.0
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-tailrec#4.1.0 bower_components/purescript-tailrec
├── purescript-bifunctors#4.0.0
├── purescript-effect#2.0.1
├── purescript-either#4.1.1
├── purescript-identity#4.1.0
├── purescript-maybe#4.0.1
├── purescript-partial#2.0.1
├── purescript-prelude#4.1.1
└── purescript-refs#4.1.0

purescript-newtype#3.0.0 bower_components/purescript-newtype
└── purescript-prelude#4.1.1

purescript-tuples#5.1.0 bower_components/purescript-tuples
├── purescript-bifunctors#4.0.0
├── purescript-control#4.2.0
├── purescript-distributive#4.0.0
├── purescript-foldable-traversable#4.1.1
├── purescript-invariant#4.1.0
├── purescript-maybe#4.0.1
├── purescript-newtype#3.0.0
├── purescript-prelude#4.1.1
└── purescript-type-equality#3.0.0

purescript-control#4.2.0 bower_components/purescript-control
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-unfoldable#4.0.2 bower_components/purescript-unfoldable
├── purescript-foldable-traversable#4.1.1
├── purescript-maybe#4.0.1
├── purescript-partial#2.0.1
├── purescript-prelude#4.1.1
└── purescript-tuples#5.1.0

purescript-partial#2.0.1 bower_components/purescript-partial

purescript-invariant#4.1.0 bower_components/purescript-invariant
└── purescript-prelude#4.1.1

purescript-orders#4.0.0 bower_components/purescript-orders
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-either#4.1.1 bower_components/purescript-either
├── purescript-bifunctors#4.0.0
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-invariant#4.1.0
├── purescript-maybe#4.0.1
└── purescript-prelude#4.1.1

purescript-identity#4.1.0 bower_components/purescript-identity
├── purescript-control#4.2.0
├── purescript-foldable-traversable#4.1.1
├── purescript-invariant#4.1.0
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-distributive#4.0.0 bower_components/purescript-distributive
├── purescript-identity#4.1.0
├── purescript-newtype#3.0.0
└── purescript-prelude#4.1.1

purescript-type-equality#3.0.0 bower_components/purescript-type-equality

purescript-refs#4.1.0 bower_components/purescript-refs
├── purescript-effect#2.0.1
└── purescript-prelude#4.1.1
ITSG003877-MAC:03-azure-funcs vikram.krishnan$ pulp build --skip-entry-point
* Building project in /Users/vikram.krishnan/Desktop/azure/scripts/03-azure-funcs
Compiling Data.Traversable.Accum
Compiling Type.Equality
Compiling Partial
Compiling Control.Lazy
Compiling Data.Bifunctor
Compiling Control.Extend
Compiling Data.Functor.Invariant
Compiling Data.Newtype
Compiling Control.Alt
Compiling Effect.Ref
Compiling Data.Traversable.Accum.Internal
Compiling Partial.Unsafe
Compiling Control.Biapply
Compiling Control.Plus
Compiling Control.Alternative
Compiling Control.Comonad
Compiling Control.MonadZero
Compiling Control.Biapplicative
Compiling Control.MonadPlus
Compiling Data.Bifunctor.Product
Compiling Data.Maybe
Compiling Data.Bifunctor.Clown
Compiling Data.Bifunctor.Join
Compiling Data.Bifunctor.Wrap
Compiling Data.Bifunctor.Joker
Compiling Data.Bifunctor.Flip
Compiling Data.Ord.Max
Compiling Data.Ord.Min
Compiling Data.Ord.Down
Compiling Data.Monoid.Alternate
Compiling Data.Maybe.Last
Compiling Data.Maybe.First
Compiling Data.FunctorWithIndex
Compiling Data.Foldable
Compiling Data.Traversable
Compiling Data.Semigroup.Foldable
Compiling Data.Bifoldable
Compiling Data.FoldableWithIndex
Compiling Data.Semigroup.Traversable
Compiling Data.Bitraversable
Compiling Data.TraversableWithIndex
Compiling Data.Identity
Compiling Data.Either
Compiling Data.Lazy
Compiling Data.Distributive
Compiling Data.Tuple
Compiling Data.Either.Inject
Compiling Control.Monad.Rec.Class
Compiling Data.Either.Nested
Compiling Data.Unfoldable1
Compiling Data.Tuple.Nested
Compiling Data.Unfoldable
Compiling Data.NonEmpty
Compiling Data.List.Types
Compiling Data.List.Lazy.Types
Compiling Data.List
Compiling Data.List.Lazy
Compiling Main
Compiling Data.List.Partial
Compiling Data.List.NonEmpty
Compiling Data.List.ZipList
Compiling Data.List.Lazy.NonEmpty
* Build successful.
ITSG003877-MAC:03-azure-funcs vikram.krishnan$ func host start

                  %%%%%%
                 %%%%%%
            @   %%%%%%    @
          @@   %%%%%%      @@
       @@@    %%%%%%%%%%%    @@@
     @@      %%%%%%%%%%        @@
       @@         %%%%       @@
         @@      %%%       @@
           @@    %%      @@
                %%
                %

Azure Functions Core Tools (2.7.1948 Commit hash: 29a0626ded3ae99c4111f66763f27bb9fb564103)
Function Runtime Version: 2.0.12888.0
[12/03/2019 10:00:31] Building host: startup suppressed: 'False', configuration suppressed: 'False', startup operation id: 'ff5f8c58-7d72-4957-86c2-a9f69eabb801'
[12/03/2019 10:00:31] Reading host configuration file '/Users/vikram.krishnan/Desktop/azure/scripts/03-azure-funcs/host.json'
[12/03/2019 10:00:31] Host configuration file read:
[12/03/2019 10:00:31] {
[12/03/2019 10:00:31]   "version": "2.0"
[12/03/2019 10:00:31] }
[12/03/2019 10:00:31] Reading functions metadata
[12/03/2019 10:00:31] 1 functions found
[12/03/2019 10:00:31] Initializing Warmup Extension.
[12/03/2019 10:00:31] Initializing Host. OperationId: 'ff5f8c58-7d72-4957-86c2-a9f69eabb801'.
[12/03/2019 10:00:31] Host initialization: ConsecutiveErrors=0, StartupCount=1, OperationId=ff5f8c58-7d72-4957-86c2-a9f69eabb801
[12/03/2019 10:00:31] LoggerFilterOptions
[12/03/2019 10:00:31] {
[12/03/2019 10:00:31]   "MinLevel": "None",
[12/03/2019 10:00:31]   "Rules": [
[12/03/2019 10:00:31]     {
[12/03/2019 10:00:31]       "ProviderName": null,
[12/03/2019 10:00:31]       "CategoryName": null,
[12/03/2019 10:00:31]       "LogLevel": null,
[12/03/2019 10:00:31]       "Filter": "<AddFilter>b__0"
[12/03/2019 10:00:31]     },
[12/03/2019 10:00:31]     {
[12/03/2019 10:00:31]       "ProviderName": "Microsoft.Azure.WebJobs.Script.WebHost.Diagnostics.SystemLoggerProvider",
[12/03/2019 10:00:31]       "CategoryName": null,
[12/03/2019 10:00:31]       "LogLevel": "None",
[12/03/2019 10:00:31]       "Filter": null
[12/03/2019 10:00:31]     },
[12/03/2019 10:00:31]     {
[12/03/2019 10:00:31]       "ProviderName": "Microsoft.Azure.WebJobs.Script.WebHost.Diagnostics.SystemLoggerProvider",
[12/03/2019 10:00:31]       "CategoryName": null,
[12/03/2019 10:00:31]       "LogLevel": null,
[12/03/2019 10:00:31]       "Filter": "<AddFilter>b__0"
[12/03/2019 10:00:31]     }
[12/03/2019 10:00:31]   ]
[12/03/2019 10:00:31] }
[12/03/2019 10:00:31] FunctionResultAggregatorOptions
[12/03/2019 10:00:31] {
[12/03/2019 10:00:31]   "BatchSize": 1000,
[12/03/2019 10:00:31]   "FlushTimeout": "00:00:30",
[12/03/2019 10:00:31]   "IsEnabled": true
[12/03/2019 10:00:31] }
[12/03/2019 10:00:31] SingletonOptions
[12/03/2019 10:00:31] {
[12/03/2019 10:00:31]   "LockPeriod": "00:00:15",
[12/03/2019 10:00:31]   "ListenerLockPeriod": "00:00:15",
[12/03/2019 10:00:31]   "LockAcquisitionTimeout": "10675199.02:48:05.4775807",
[12/03/2019 10:00:31]   "LockAcquisitionPollingInterval": "00:00:05",
[12/03/2019 10:00:31]   "ListenerLockRecoveryPollingInterval": "00:01:00"
[12/03/2019 10:00:31] }
[12/03/2019 10:00:31] HttpOptions
[12/03/2019 10:00:31] {
[12/03/2019 10:00:31]   "DynamicThrottlesEnabled": false,
[12/03/2019 10:00:31]   "MaxConcurrentRequests": -1,
[12/03/2019 10:00:31]   "MaxOutstandingRequests": -1,
[12/03/2019 10:00:31]   "RoutePrefix": "api"
[12/03/2019 10:00:31] }
[12/03/2019 10:00:31] Starting JobHost
[12/03/2019 10:00:32] Starting Host (HostId=itsg003877mac-1840752573, InstanceId=aa4e09fc-93df-4f76-85b4-3a10b85db516, Version=2.0.12888.0, ProcessId=37264, AppDomainId=1, InDebugMode=False, InDiagnosticMode=False, FunctionsExtensionVersion=(null))
[12/03/2019 10:00:32] Loading functions metadata
[12/03/2019 10:00:32] 1 functions loaded
[12/03/2019 10:00:32] Starting worker process:node  "/usr/local/Cellar/azure-functions-core-tools/2.7.1948/workers/node/dist/src/nodejsWorker.js" --host 127.0.0.1 --port 59283 --workerId cebd746b-5f8c-4d07-8c1b-62992cf30cc0 --requestId a7068d10-752f-4aca-8266-d647fe1f5b0a --grpcMaxMessageLength 134217728
[12/03/2019 10:00:32] node process with Id=37265 started
[12/03/2019 10:00:32] Generating 1 job function(s)
[12/03/2019 10:00:32] Found the following functions:
[12/03/2019 10:00:32] Host.Functions.MyHttpTrigger
[12/03/2019 10:00:32] 
[12/03/2019 10:00:32] Initializing function HTTP routes
[12/03/2019 10:00:32] Mapped function route 'api/MyHttpTrigger' [get,post] to 'MyHttpTrigger'
[12/03/2019 10:00:32] 
[12/03/2019 10:00:32] Host initialized (227ms)
[12/03/2019 10:00:32] Host started (233ms)
[12/03/2019 10:00:32] Job host started
[12/03/2019 10:00:32] [warn] The Node.js version you are using (v12.13.1) is not fully supported by Azure Functions V2. We recommend using one the following major versions: 8, 10.
[12/03/2019 10:00:32] Worker cebd746b-5f8c-4d07-8c1b-62992cf30cc0 connecting on 127.0.0.1:59283
Hosting environment: Production
Content root path: /Users/vikram.krishnan/Desktop/azure/scripts/03-azure-funcs
Now listening on: http://0.0.0.0:7071
Application started. Press Ctrl+C to shut down.

Http Functions:

	MyHttpTrigger: [GET,POST] http://localhost:7071/api/MyHttpTrigger

[12/03/2019 10:00:37] Host lock lease acquired by instance ID '000000000000000000000000C7C37F3D'.
[12/03/2019 10:00:45] Executing HTTP request: {
[12/03/2019 10:00:45]   "requestId": "bc66ad2e-b0cf-4b65-9840-025558a4c3b3",
[12/03/2019 10:00:45]   "method": "GET",
[12/03/2019 10:00:45]   "uri": "/api/MyHttpTrigger"
[12/03/2019 10:00:45] }
[12/03/2019 10:00:45] Executing 'Functions.MyHttpTrigger' (Reason='This function was programmatically called via the host APIs.', Id=0df7ca65-55f5-4428-9bb6-ec769be22964)
[12/03/2019 10:00:45] JavaScript HTTP trigger function processed a request.
[12/03/2019 10:00:45] {
[12/03/2019 10:00:45]   ns: [Function: ns],
[12/03/2019 10:00:45]   multiples: [Function: multiples],
[12/03/2019 10:00:45]   answer: [Function: answer]
[12/03/2019 10:00:45] }
[12/03/2019 10:00:45] Executed 'Functions.MyHttpTrigger' (Succeeded, Id=0df7ca65-55f5-4428-9bb6-ec769be22964)
[12/03/2019 10:00:45] Executed HTTP request: {
[12/03/2019 10:00:45]   "requestId": "bc66ad2e-b0cf-4b65-9840-025558a4c3b3",
[12/03/2019 10:00:45]   "method": "GET",
[12/03/2019 10:00:45]   "uri": "/api/MyHttpTrigger",
[12/03/2019 10:00:45]   "identities": [
[12/03/2019 10:00:45]     {
[12/03/2019 10:00:45]       "type": "WebJobsAuthLevel",
[12/03/2019 10:00:45]       "level": "Admin"
[12/03/2019 10:00:45]     }
[12/03/2019 10:00:45]   ],
[12/03/2019 10:00:45]   "status": 200,
[12/03/2019 10:00:45]   "duration": 533
[12/03/2019 10:00:45] }

