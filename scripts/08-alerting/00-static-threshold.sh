#!/bin/bash

az group create --location eastus --name MyResourceGroup

vmDetails=$(az vm create \
  --resource-group MyResourceGroup \
  --name myVM \
  --image UbuntuLTS \
  --admin-username azureuser \
  --generate-ssh-keys)

az group deployment create \
    --name ActionGroup \
    --resource-group MyResourceGroup \
    --template-file alertactiongroup.json \
    --parameters @alertactiongroup.parameters.json

az group deployment create \
    --name AlertDeployment \
    --resource-group MyResourceGroup \
    --template-file simplestaticmetricalert.json \
    --parameters @simplestaticmetricalert.parameters.json

az group delete -n MyResourceGroup
