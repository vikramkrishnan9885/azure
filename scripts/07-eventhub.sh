#!/bin/bash
#NEW_UUID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
SUBSCRIPTION='a4a8c613-6bb3-4a55-90cd-be8a39ef320e'
RG_NAME="myResourceGroup"
RG_LOCATION="eastus"
EH_NAMESPACE='myEHNamespace-'$RANDOM
echo $EH_NAMESPACE
EH_NAME='myEH'
SERVICE_BUS_RULE_ID="/subscriptions/"$SUBSCRIPTION"/resourceGroups/"$RG_NAME"/providers/Microsoft.EventHub/namespaces/"$EH_NAMESPACE"/authorizationrules/RootManageSharedAccessKey"
AUTH_RULE_NAME='myAuthRule'

az account set --subscription $SUBSCRIPTION
az group create --name $RG_NAME --location $RG_LOCATION
az eventhubs namespace create --name $EH_NAMESPACE --resource-group $RG_NAME -l $RG_LOCATION
az eventhubs eventhub create --name $EH_NAME --resource-group $RG_NAME --namespace-name $EH_NAMESPACE
az eventhubs eventhub authorization-rule create --resource-group $RG_NAME --namespace-name $EH_NAMESPACE --eventhub-name $EH_NAME --name $AUTH_RULE_NAME --rights Listen Manage Send 

az monitor log-profiles create --categories "Delete" "Write" "Action" --days 7 --enabled true --location null --locations "global" "eastus" "westus" --name "MyLogProfile" --service-bus-rule-id $SERVICE_BUS_RULE_ID