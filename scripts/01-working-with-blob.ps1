Import-Module Az

# CREATE RESOURCE GROUP
$rgParams = @{
 Name="myResourceGroup"
 Location="eastus"
}
Write-Output "********************************************************"
Write-Output "Creating Resource Group with name"
Write-Output $rgParams.Name
Write-Output "********************************************************"
New-AzResourceGroup @rgParams

# CREATE A STORAGE ACCOUNT
# Use the following example to create a storage account 
# with locally redundant storage (LRS) and blob encryption (enabled by default).
$storageAccountNameGuid = [System.guid]::NewGuid().toString()
$storageAccountNameGuidDashRemoved = $storageAccountNameGuid.replace("-","")
$storageAccountName = $storageAccountNameGuidDashRemoved.SubString(0,18)
Write-Output "********************************************************"
Write-Output "Creating Storage Account with name"
Write-Output $storageAccountName
Write-Output "********************************************************"
$storageAccount = New-AzStorageAccount -ResourceGroupName $rgParams.Name -Name $storageAccountName -SkuName Standard_LRS -Location $rgParams.Location 
$ctx = $storageAccount.Context

# CREATE A CONTAINER
# Blobs are always uploaded into a container. 
# You can organize groups of blobs like the way you organize your 
# files on your computer in folders.
# Set the permissions to blob to allow public access of the files.
$containerName = "quickstartblobs"
New-AzStorageContainer -Name $containerName -Context $ctx -Permission blob

# UPLOAD FILES
# To upload a file to a block blob, get a container reference, 
# then get a reference to the block blob in that container. 
# Once you have the blob reference, you can upload data to it 
# by using Set-AzStorageBlobContent.
 
# upload a file
Set-AzStorageBlobContent -File "/Users/vikram.krishnan/Desktop/azure/scripts/file1.txt" -Container $containerName -Blob "file001.txt" -Context $ctx 

# upload another file
Set-AzStorageBlobContent -File "/Users/vikram.krishnan/Desktop/azure/scripts/file2.txt" -Container $containerName -Blob "file002.txt" -Context $ctx

# List blobs in container
Get-AzStorageBlob -Container $ContainerName -Context $ctx | Select-Object Name

# DOWNLOAD FILES
# download first blob
Get-AzStorageBlobContent -Blob "file001.txt" -Container $containerName -Destination "/Users/vikram.krishnan/Desktop/" -Context $ctx 

# download another blob
Get-AzStorageBlobContent -Blob "file002.txt" -Container $containerName -Destination "/Users/vikram.krishnan/Desktop/" -Context $ctx

# CLEAN UP
$job = Remove-AzResourceGroup -Name $rgParams.Name -Force -AsJob
# To wait until the deletion is complete
Wait-Job -Id $job.Id