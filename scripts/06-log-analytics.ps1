############################################################################
#
# Create and configure a Log Analytics Workspace
#
############################################################################

$ResourceGroup = "oms-example"
# workspace names need to be unique across all Azure subscriptions
$WorkspaceName = "log-analytics-" + (Get-Random -Maximum 99999)
$Location = "westeurope"

# List of solutions to enable
$Solutions = "Security", "Updates", "SQLAssessment"

# Saved Searches to import
# THESE ARE LOG ANALYTICS - EQ. ELASTICSEARCH QUERY
$ExportedSearches = @"
[
    {
        "Category":  "My Saved Searches",
        "DisplayName":  "WAD Events (All)",
        "Query":  "Type=Event SourceSystem:AzureStorage ",
        "Version":  1
    },
    {
        "Category":  "My Saved Searches",
        "DisplayName":  "Current Disk Queue Length",
        "Query":  "Perf | where ObjectName == 'LogicalDisk' and CounterName == 'Current Disk Queue Length' and InstanceName == 'C:'",
        "Version":  1
    }
]
"@ | ConvertFrom-Json
# Queries
# https://docs.microsoft.com/en-us/azure/azure-monitor/platform/app-insights-metrics#server-exceptions-exceptionsserver

# Custom Log to collect
$CustomLog = @"
{
    "customLogName": "sampleCustomLog1",
    "description": "Example custom log datasource",
    "inputs": [
        {
            "location": {
            "fileSystemLocations": {
                "windowsFileTypeLogPaths": [ "e:\\iis5\\*.log" ],
                "linuxFileTypeLogPaths": [ "/var/logs" ]
                }
            },
        "recordDelimiter": {
            "regexDelimiter": {
                "pattern": "\\n",
                "matchIndex": 0,
                "matchIndexSpecified": true,
                "numberedGroup": null
                }
            }
        }
    ],
    "extractions": [
        {
            "extractionName": "TimeGenerated",
            "extractionType": "DateTime",
            "extractionProperties": {
                "dateTimeExtraction": {
                    "regex": null,
                    "joinStringRegex": null
                    }
                }
            }
        ]
    }
"@

$rg=$null
# Create the resource group if needed
try {
    $rg =Get-AzResourceGroup -Name $ResourceGroup -ErrorAction Stop
} catch {
    $rg =New-AzResourceGroup -Name $ResourceGroup -Location $Location
}
Write-Output "**************************************************************"
Write-Output "RESOURCE GROUP"
Write-Output "**************************************************************"
Write-Output ($rg | Select-Object ResourceId).ResourceId.replace("`n","").replace("`r","")
$resourceGroupId = (($rg | Select-Object ResourceId).ResourceId| Out-String).replace("`n","").replace("`r","")

# Create the workspace
$newWS = New-AzOperationalInsightsWorkspace -Location $Location -Name $WorkspaceName -Sku Standard -ResourceGroupName $ResourceGroup
$workspaceId = (($newWS | Select-Object ResourceId).ResourceId|Out-String).replace("`n","").replace("`r","")
Write-Output "**************************************************************"
Write-Output "WORKSPACE"
Write-Output "**************************************************************"
Write-Output $workspaceId

# List all solutions and their installation status
Write-Output "**************************************************************"
Write-Output "ALL SOLUTIONS"
Write-Output "**************************************************************"
(Get-AzOperationalInsightsIntelligencePack -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName) | Out-File 'All-Solutions.txt'
Write-Output "**************************************************************"

# Add solutions
foreach ($solution in $Solutions) {
    Set-AzOperationalInsightsIntelligencePack -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName -IntelligencePackName $solution -Enabled $true
}

# List enabled solutions
Write-Output "**************************************************************"
Write-Output "ENABLED SOLUTIONS"
Write-Output "**************************************************************"
(Get-AzOperationalInsightsIntelligencePack -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName).Where({($_.enabled -eq $true)}) | Out-File 'Enabled-Solutions.txt'
Write-Output "**************************************************************"

# Import Saved Searches
foreach ($search in $ExportedSearches) {
    $id = $search.Category + "|" + $search.DisplayName
    New-AzOperationalInsightsSavedSearch -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName -SavedSearchId $id -DisplayName $search.DisplayName -Category $search.Category -Query $search.Query -Version $search.Version
}

# Export Saved Searches
Write-Output "**************************************************************"
Write-Output "SAVED SEARCHES"
Write-Output "**************************************************************"
(Get-AzOperationalInsightsSavedSearch -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName).Value.Properties | ConvertTo-Json | Out-File 'SavedSearches.json'
Write-Output "**************************************************************"

# Create Computer Group based on a query
New-AzOperationalInsightsComputerGroup -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName -SavedSearchId "My Web Servers" -DisplayName "Web Servers" -Category "My Saved Searches" -Query "Computer=""web*"" | distinct Computer" -Version 1

# Enable IIS Log Collection using agent
Enable-AzOperationalInsightsIISLogCollection -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName

# Linux Perf
New-AzOperationalInsightsLinuxPerformanceObjectDataSource -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName -ObjectName "Logical Disk" -InstanceName "*"  -CounterNames @("% Used Inodes", "Free Megabytes", "% Used Space", "Disk Transfers/sec", "Disk Reads/sec", "Disk Reads/sec", "Disk Writes/sec") -IntervalSeconds 20  -Name "Example Linux Disk Performance Counters"
Enable-AzOperationalInsightsLinuxPerformanceCollection -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName

# Linux Syslog
New-AzOperationalInsightsLinuxSyslogDataSource -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName -Facility "kern" -CollectEmergency -CollectAlert -CollectCritical -CollectError -CollectWarning -Name "Example kernel syslog collection"
Enable-AzOperationalInsightsLinuxSyslogCollection -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName

# Windows Event
New-AzOperationalInsightsWindowsEventDataSource -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName -EventLogName "Application" -CollectErrors -CollectWarnings -Name "Example Application Event Log"

# Windows Perf
New-AzOperationalInsightsWindowsPerformanceCounterDataSource -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName -ObjectName "Memory" -InstanceName "*" -CounterName "Available MBytes" -IntervalSeconds 20 -Name "Example Windows Performance Counter"

# Custom Logs
New-AzOperationalInsightsCustomLogDataSource -ResourceGroupName $ResourceGroup -WorkspaceName $WorkspaceName -CustomLogRawJson "$CustomLog" -Name "Example Custom Log Collection"

# Configuring Log Analytics to send Azure diagnostics
# Set-AzDiagnosticSetting -ResourceId $resourceGroupId -WorkspaceId $workspaceId -Enabled $true
Write-Output "**************************************************************"
Write-Output "Writing cmd to shell script and running it"
Write-Output "**************************************************************"
"az monitor log-analytics workspace create -g $ResourceGroup -n $WorkspaceName" | Out-File 'monitor.sh'
bash monitor.sh

# CLEAN UP
# $job = Remove-AzResourceGroup -Name $ResourceGroup -Force -AsJob
# To wait until the deletion is complete
# Wait-Job -Id $job.Id