#!/bin/bash

az group create --location eastus --name myRG

az extension add -n application-insights

az monitor app-insights component create --app demoApp --location westus2 --kind web -g myRG --application-type web

az group deployment create \
    --name LogAlertDeployment \
    --resource-group myRG \
    --template-file sample-log-alert.json

az group delete -n myRG