#!/bin/bash

# Create a resource group
az group create --location eastus --name MyResourceGroup
# you can use -l and -n instead

# Create virtual machine
# The --generate-ssh-keys parameter is used to automatically generate an SSH key, 
# and put it in the default key location (~/.ssh). To use a specific set of keys 
# instead, use the --ssh-key-value option.
vmDetails=$(az vm create \
  --resource-group MyResourceGroup \
  --name myVM \
  --image UbuntuLTS \
  --admin-username azureuser \
  --generate-ssh-keys)

publicIpAddress=$(echo ${vmDetails}|jq -r '.publicIpAddress')
resourceId=$(echo ${vmDetails}| jq -r '.id')

echo "========================================================================="
echo "VM DETAILS"
echo "========================================================================="
echo $vmDetails

echo "========================================================================="
echo "PUBLIC IP ADDRESS"
echo "========================================================================="
echo $publicIpAddress
echo "========================================================================="
echo "VM ID"
echo "========================================================================="
echo $resourceId
echo "========================================================================="


# Open port 80 for web traffic
az vm open-port --port 80 --resource-group MyResourceGroup --name myVM

echo "========================================================================="
echo "PERCENTAGE CPU METRICS: METHOD 1"
echo "========================================================================="
az monitor metrics list --resource myVM --resource-group MyResourceGroup --resource-type Microsoft.Compute/virtualMachines  --metric "Percentage CPU"

echo "========================================================================="
echo "PERCENTAGE CPU METRICS: METHOD 2"
echo "========================================================================="
az monitor metrics list --resource $resourceId --metric "Percentage CPU"