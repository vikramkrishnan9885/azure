# Import Az module. Remember not to import AzureRM otherwise it will result in conflicts
Import-Module Az

# Setup resorce group. Notice the use of splatting
$rgParams = @{
    Name='TutorialResources'
    Location='eastus'
}
New-AzResourceGroup @rgParams

# Get credentials from user
# $cred = Get-Credential -Message "Enter a username and password for the virtual machine."
$User = "User01"
$PWord = ConvertTo-SecureString -String "P@sSwOrd" -AsPlainText -Force
$cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $PWord

# Create a VM and print out some parameters
$vmParams = @{
   ResourceGroupName = 'TutorialResources'
   Name = 'TutorialVM1'
   Location = 'eastus'
   ImageName = 'Win2016Datacenter'
   PublicIpAddressName = 'tutorialPublicIp'
   Credential = $cred
   OpenPorts = 3389
 }
$newVM1 = New-AzVM @vmParams
$P = $newVM1.OSProfile | Select-Object ComputerName,AdminUserName
Write-Output $P

Write-Output "====================================================================="
$VMID = $newVM1 | Select-Object VMID
Write-Output $VMID
Write-Output "====================================================================="
Write-Output $newVM1
Write-Output "====================================================================="
$IdObj = ($newVM1 | Select-Object ID).Id
$IdStr =  ($IdObj | Out-String).replace("`n","").replace("`r","")
$ID = $IdStr
Write-Output $ID
Write-Output "====================================================================="
(Get-AzMetric -ResourceId $ID -TimeGrain 00:01:00 -DetailedOutput).Data
Write-Output "====================================================================="
Get-AzMetricDefinition -ResourceId $ID -DetailedOutput
Write-Output "====================================================================="


# Remove the RG
$job= Remove-AzResourceGroup -Name TutorialResources -Force -AsJob
# To wait until the deletion is complete
Wait-Job -Id $job.Id