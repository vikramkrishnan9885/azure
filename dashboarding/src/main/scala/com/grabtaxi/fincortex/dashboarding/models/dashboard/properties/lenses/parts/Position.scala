package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts

import com.google.gson.Gson

case class Position(x:Int=0, y:Int=0, rowSpan:Int=4, colSpan:Int=6){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}
object Position {
  val default = new Position()
}
