package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters.msportalfxtimerange.{DisplayCache, MsPortalFxTimeRangeModel}

case class MsPortalFxTimeRange(model:MsPortalFxTimeRangeModel, displayCache: DisplayCache, filteredPartIds:Array[String])
