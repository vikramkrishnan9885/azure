package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata

import com.google.gson.Gson
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.Chart

case class LensPartMetadataSettings(content:OuterContent){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}

trait OuterContent

case class OuterContentWithInnerOptions(options:InnerOptions) extends OuterContent {
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}

case class InnerOptions(chart:Chart)  {
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}

trait InnerOptionsGenerator {
  def generate(innerOptions: InnerOptions)
}

object InnerOptionsGenerator {
  
}

case class OuterContentWithInnerSettings(settings:InnerSettings=InnerSettings.default) extends OuterContent {
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}
object OuterContentWithInnerSettings{
  val default = new OuterContentWithInnerSettings()
}

case class InnerSettings(
  content:Option[String]
  , title:String="My Title"
  , subtitle:String="My Subtitle"
)  {
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}

object InnerSettings {
  val default = new InnerSettings(None)
}