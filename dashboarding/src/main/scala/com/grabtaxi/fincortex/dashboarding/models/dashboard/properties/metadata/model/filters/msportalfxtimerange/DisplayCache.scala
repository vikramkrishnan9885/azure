package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters.msportalfxtimerange

case class DisplayCache(name:String="UTC Time", value:String="Past 24 hours")
