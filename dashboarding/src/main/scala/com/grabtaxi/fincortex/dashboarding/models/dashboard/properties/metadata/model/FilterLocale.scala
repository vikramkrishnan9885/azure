package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model

case class FilterLocale(value:String="en-us")
object FilterLocale {
  val default = new FilterLocale()
}
