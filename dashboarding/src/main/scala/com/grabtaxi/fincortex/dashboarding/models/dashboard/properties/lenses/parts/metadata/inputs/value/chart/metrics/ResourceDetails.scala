package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart.metrics

import com.google.gson.Gson
import com.typesafe.config.{Config, ConfigFactory}

case class ResourceDetails(
  subscriptionId: String
 , resourceGroupName: String
 , resourceType: String
 , resourceName: String
){
  def getResourceMetadataString: String = {
    val resourceTypeWithProvider = ResourceDetails.RESOURCE_PROVIDER_MAPPER(resourceType)
    val returnString = s"/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/$resourceTypeWithProvider/$resourceName"
    returnString
  }

  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}

object ResourceDetails {
  val RESOURCE_PROVIDER_MAPPER = Map(
    "BATCH_accounts" -> "Microsoft.Batch/batchAccounts",
    "BATCH_operations" -> "Microsoft.Batch/operations",
    "COMPUTE_availabilitySets" -> "Microsoft.Compute/availabilitySets",
    "COMPUTE_virtualMachines" -> "Microsoft.Compute/virtualMachines",
    "COMPUTE_virtualMachineScaleSets" ->"Microsoft.Compute/virtualMachineScaleSets",
    "COMPUTE_operations" -> "Microsoft.Compute/operations",
    "COMPUTE_disks" -> "Microsoft.Compute/disks",
    "COMPUTE_snapshots" -> "Microsoft.Compute/snapshots",
    "COMPUTE_images" -> "Microsoft.Compute/images",
    "ADF_dataFactories" -> "Microsoft.DataFactory/dataFactories",
    "ADF_factories" ->"Microsoft.DataFactory/factories",
    "ADF_operations" -> "Microsoft.DataFactory/operations",
    "EVENTHUB_namespaces" -> "Microsoft.EventHub/namespaces",
    "EVENTHUB_clusters" -> "Microsoft.EventHub/clusters",
    "EVENTHUB_eventhubs" -> "Microsoft.EventHub/namespaces/eventhubs",
    "EVENTHUB_operations" -> "Microsoft.EventHub/operations",
    "NETWORK_virtualNetworks" -> "Microsoft.Network/virtualNetworks",
    "NETWORK_publicIPAddresses" -> "Microsoft.Network/publicIPAddresses",
    "NETWORK_networkInterfaces" ->  "Microsoft.Network/networkInterfaces",
    "NETWORK_privateEndpoints" ->  "Microsoft.Network/privateEndpoints",
    "NETWORK_loadBalancers" -> "Microsoft.Network/loadBalancers",
    "NETWORK_networkSecurityGroups" -> "Microsoft.Network/networkSecurityGroups",
    "NETWORK_routeTables" -> "Microsoft.Network/routeTables",
    "NETWORK_networkWatchers" -> "Microsoft.Network/networkWatchers",
    "NETWORK_virtualNetworkGateways" -> "Microsoft.Network/virtualNetworkGateways",
    "NETWORK_localNetworkGateways" -> "Microsoft.Network/localNetworkGateways",
    "NETWORK_connections" -> "Microsoft.Network/connections",
    "NETWORK_applicationGateways" -> "Microsoft.Network/applicationGateways",
    "NETWORK_vpnGateways" -> "Microsoft.Network/vpnGateways",
    "NETWORK_p2sVpnGateways" -> "Microsoft.Network/p2sVpnGateways",
    "NETWORK_expressRouteGateways" -> "Microsoft.Network/expressRouteGateways",
    "STORAGE_storageAccounts" -> "Microsoft.Storage/storageAccounts",
    "STORAGE_operations" -> "Microsoft.Storage/operations",
    "STORAGE_usages" -> "Microsoft.Storage/usages"
  )
}




trait ResourceDetailsGenerator {
  def generate():ResourceDetails
}

object ResourceDetailsGenerator {
  def apply(s: String
            , confFileName: Option[String] = None
            , env: Option[String] = None
            , subscriptionId: Option[String] = None
            , resourceGroupName: Option[String] = None
            , resourceType: Option[String] = None
            , resourceName: Option[String] = None
           ): ResourceDetailsGenerator = s match {
    case "mock" => return new ResourceDetailsGeneratorMock
    case "file" => return new ResourceDetailsGeneratorFileImpl(confFileName.get, env.get)
    case "direct" => return new ResourceDetailsGeneratorDirectImpl(subscriptionId.get, resourceGroupName.get, resourceType.get, resourceName.get)
  }
  
  private class ResourceDetailsGeneratorMock extends ResourceDetailsGenerator {
    override def generate(): ResourceDetails = {
      new ResourceDetails(
        "mockSubscriptionId",
        "mockResourceGroupName",
        "mockResourceType",
        "mockResourceName"
      )
    }
  }

  private class ResourceDetailsGeneratorFileImpl(confFileName:String, env:String) extends ResourceDetailsGenerator {
    override def generate(): ResourceDetails = {
      val config: Config = ConfigFactory.load(confFileName)
      val subscriptionId: String = config.getString(s"$env.subscriptionId")
      val resourceGroupName: String = config.getString(s"$env.resourceGroupName")
      val resourceType: String = config.getString(s"$env.resourceType")
      val resourceName: String = config.getString(s"$env.resourceName")
      val resourceDetails = new ResourceDetails(
        subscriptionId
        , resourceGroupName
        , resourceType
        , resourceName
      )
      resourceDetails
    }
  }

  private class ResourceDetailsGeneratorDirectImpl(subscriptionId: String, resourceGroupName: String, resourceType: String, resourceName: String)
  extends ResourceDetailsGenerator {
    override def generate(): ResourceDetails = new ResourceDetails(subscriptionId, resourceGroupName,resourceType, resourceName)
  }
}