package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.{FilterLocale, Filters, TimeRange}

case class MetadataModel(timeRange:TimeRange, filters:Filters, filterLocale:FilterLocale=FilterLocale.default)
