package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs

import com.google.gson.Gson
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.{Chart, ChartGenerator}

case class InputsValue (chart:Chart){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}

trait InputsValueGenerator {
  def generate(): InputsValue
}
object InputsValueGenerator {

  def apply(s: String, chartGenerator: Option[ChartGenerator]=None):InputsValueGenerator = s match {
    case "mock" => return new InputsValueGeneratorMock
    case _ => return new InputsValueGeneratorImpl(chartGenerator.get)
  }

  private class InputsValueGeneratorMock extends InputsValueGenerator{
    override def generate(): InputsValue = new InputsValue(ChartGenerator("mock").generate())
  }

  private class InputsValueGeneratorImpl(chartGenerator: ChartGenerator) extends InputsValueGenerator {
    override def generate(): InputsValue = new InputsValue(chartGenerator.generate())
  }
}
