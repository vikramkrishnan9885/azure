package com.grabtaxi.fincortex.dashboarding.models

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.Lenses
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.LensPartMetadata
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.TimeRange
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters.{FiltersValue, MsPortalFxTimeRange}
import com.grabtaxi.fincortex.dashboarding.models.dashboard.{Properties, Tags}
import com.grabtaxi.fincortex.dashboarding.utils.{CustomJsonSerializer, DateTimeFormats}
import org.json4s.FieldSerializer
import org.json4s.FieldSerializer.renameTo

case class Dashboard (
 properties: Properties
 , name:String
 , tags:Tags
 , entityType:String = "Microsoft.Portal/dashboards"
 , location:String = "southeastasia"
 , apiVersion:String = "2015-08-01-preview"
) {
  require(tags.hiddenTitle == name)

  override def toString: String = DashboardJsonSerializer.toJson(this)
}
object Dashboard {
  val rename = FieldSerializer[Dashboard](
    renameTo("entityType", "type")
  )
  val formats = DateTimeFormats.format +
    Tags.rename +
    FiltersValue.rename +
    TimeRange.rename +
    Lenses.rename +
    LensPartMetadata.rename +
    rename
}

trait DashboardJsonSerializer extends CustomJsonSerializer[Dashboard]{
  val dashboardFormats = Dashboard.formats

  def fromJson(jsonString: String)
    (implicit m: Manifest[Dashboard])
  : Option[Dashboard] = super.fromJson(jsonString)(dashboardFormats)

  def toJson(t: Dashboard): String = super.toJson(t)(dashboardFormats)
}
object DashboardJsonSerializer extends DashboardJsonSerializer
