package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses

import com.google.gson.Gson

/*
object Parts {
  type Parts = Map[String, String]
}
*/

case class Parts(id:String="0",part: Part=Part.default){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}