package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart

import com.google.gson.Gson

case class ChartVisualization(chartType:Int){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}
object ChartVisualization {
  val CHART_TYPE_MAPPER = Map (
    "bar" -> 1,
    "line" -> 2,
    "area" -> 3,
    "scatter" -> 4
  )

  val DEFAULT = new ChartVisualization(CHART_TYPE_MAPPER("line"))
}


trait ChartVisualizationGenerator {
  def generate(): ChartVisualization
}

object ChartVisualizationGenerator {
  def apply(s:String, chartTypeString:Option[String]=None):ChartVisualizationGenerator = s match {
    case "mock" => return new ChartVisualizationGeneratorMock
    case _ => return new ChartVisualizationGeneratorImpl(chartTypeString.get)
  }

  private class ChartVisualizationGeneratorImpl(chartTypeString:String) extends ChartVisualizationGenerator {
    override def generate(): ChartVisualization = {
      new ChartVisualization(
        ChartVisualization.CHART_TYPE_MAPPER(chartTypeString)
      )
    }
  }

  private class ChartVisualizationGeneratorMock extends ChartVisualizationGenerator {
    override def generate(): ChartVisualization = ChartVisualization.DEFAULT
  }
}