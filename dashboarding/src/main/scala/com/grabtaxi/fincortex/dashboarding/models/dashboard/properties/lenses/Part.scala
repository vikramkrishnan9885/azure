package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses

import com.google.gson.Gson
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.{LensPartMetadata, Position}

case class Part(position:Position=Position.default,
                metadata:LensPartMetadata=LensPartMetadata.default){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}
object Part {
  val default = new Part()
}
