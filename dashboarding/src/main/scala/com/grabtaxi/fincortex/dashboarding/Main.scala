package com.grabtaxi.fincortex.dashboarding

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart.ChartVisualization
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart.metrics.{MetricVisualization, MetricVisualizationGenerator}
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.{Lens, Parts}
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.MetadataModel
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters.msportalfxtimerange.{DisplayCache, MsPortalFxTimeRangeModel}
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters.{FiltersValue, MsPortalFxTimeRange}
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.{FilterLocale, Filters, TimeRange}
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.{Lenses, Metadata}
import com.grabtaxi.fincortex.dashboarding.models.dashboard.{Properties, Tags}
import com.grabtaxi.fincortex.dashboarding.models.{Dashboard, DashboardJsonSerializer}

object Main extends App{

  val name = "MyTitle"
  val tags = new Tags(name)
  val timeRangeModel = new MsPortalFxTimeRangeModel()
  val displayCache = new DisplayCache()
  val filteredPartsIds = Array("test")
  val timeRange = new TimeRange()
  val portalTimeRange = new MsPortalFxTimeRange(timeRangeModel,displayCache,filteredPartsIds)
  val values = new FiltersValue(portalTimeRange)
  val filters = new Filters(values)
  val model = new MetadataModel(timeRange, filters)
  val dashPropertiesMetadata = new Metadata(model)

  val parts = new Parts()
  //val parts = Map("string"->"AnotherString")
  val lens = new Lens()
  val lenses = new Lenses(lens)
  val dashProperties = new Properties(lenses, dashPropertiesMetadata)
  val dashboard = new Dashboard(
    dashProperties,
    name,
    tags
  )

  println(DashboardJsonSerializer.toJson(dashboard))
  println("===========================================================")
  println(ChartVisualization.CHART_TYPE_MAPPER("bar"))

  println("===========================================================")
  val x: MetricVisualization = MetricVisualizationGenerator("mock").generate()
  println(x)
}
