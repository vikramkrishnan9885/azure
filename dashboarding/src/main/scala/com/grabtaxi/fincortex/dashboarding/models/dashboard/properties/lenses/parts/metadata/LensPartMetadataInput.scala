package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata

import com.google.gson.Gson
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.{InputsValue, InputsValueGenerator}

class LensPartMetadataInput(
  value:InputsValue
  , name:String="queryInputs"
  , isOptional:Boolean=true
){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}

trait LensPartMetadataInputGenerator {
  def generate(): LensPartMetadataInput
}

object LensPartMetadataInputGenerator {

  def apply(s: String, value:Option[InputsValue]=None, name:Option[String]=None, isOptional:Option[Boolean]=None):LensPartMetadataInputGenerator = s match {
    case "mock" => return new LensPartMetadataInputGeneratorMock
    case _ => return new LensPartMetadataInputGeneratorImpl(value.get, name.get, isOptional.get)
  }

  private class LensPartMetadataInputGeneratorMock extends LensPartMetadataInputGenerator{
    override def generate(): LensPartMetadataInput = new LensPartMetadataInput(InputsValueGenerator("mock").generate())
  }

  private class LensPartMetadataInputGeneratorImpl(value:InputsValue, name:String="queryInputs", isOptional:Boolean=true) extends LensPartMetadataInputGenerator {
    override def generate(): LensPartMetadataInput = new LensPartMetadataInput(value, name, isOptional)
  }
}