package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses

import com.google.gson.Gson

case class Lens(parts:Map[String,Part]=Map("0"->Part.default), order:Int=0){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}
