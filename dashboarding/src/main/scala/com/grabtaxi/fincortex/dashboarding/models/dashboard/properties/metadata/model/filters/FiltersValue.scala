package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters

import com.grabtaxi.fincortex.dashboarding.utils.DateTimeFormats
import org.json4s.FieldSerializer.renameTo
import org.json4s.{FieldSerializer, Formats}

case class FiltersValue(msPortalFxTimeRange:MsPortalFxTimeRange)
object FiltersValue {
  val rename = FieldSerializer[FiltersValue](
    renameTo("msPortalFxTimeRange", "MsPortalFx_TimeRange")
  )

  val formats: Formats = DateTimeFormats.format + rename
}
