package com.grabtaxi.fincortex.dashboarding.utils

import java.text.SimpleDateFormat

import org.json4s.DefaultFormats
import org.json4s.ext.JodaTimeSerializers

object DateTimeFormats {
  val format = new DefaultFormats {
    override def dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
  } ++ JodaTimeSerializers.all
}
