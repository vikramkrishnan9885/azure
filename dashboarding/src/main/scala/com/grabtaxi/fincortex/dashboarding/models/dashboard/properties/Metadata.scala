package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.MetadataModel

case class Metadata(model:MetadataModel)
