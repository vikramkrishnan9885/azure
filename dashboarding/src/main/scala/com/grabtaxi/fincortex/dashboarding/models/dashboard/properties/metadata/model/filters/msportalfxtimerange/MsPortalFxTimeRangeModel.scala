package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters.msportalfxtimerange

case class MsPortalFxTimeRangeModel(format:String="utc",granularity:String="auto",relative:String="24h")
