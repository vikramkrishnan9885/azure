package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart.metrics

import com.google.gson.Gson

case class ResourceMetadata(id: String) {
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}
object ResourceMetadata {
  val DEFAULT = new ResourceMetadata("/subscriptions/mockSubscriptionId/resourceGroups/mockResourceGroupName/providers/mockResourceTypeWithProvider/mockResourceName")
}


trait ResourceMetadataGenerator {
  def generate(): ResourceMetadata
}
object ResourceMetadataGenerator {

  def apply(s:String, resourceDetailsGenerator: Option[ResourceDetailsGenerator]=None): ResourceMetadataGenerator = s match {
    case "mock" => return new ResourceMetadataGeneratorMock
    case _ => return  new ResourceMetadataGeneratorImpl(resourceDetailsGenerator.get)
  }

  private class ResourceMetadataGeneratorImpl(resourceDetailsGenerator: ResourceDetailsGenerator) extends ResourceMetadataGenerator {
    override def generate(): ResourceMetadata = {
      val returnResourceMetadata = new ResourceMetadata(resourceDetailsGenerator.generate.getResourceMetadataString)
      returnResourceMetadata
    }
  }

  private class ResourceMetadataGeneratorMock extends ResourceMetadataGenerator {
    override def generate(): ResourceMetadata = ResourceMetadata.DEFAULT
  }
}