package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart.metrics

import com.google.gson.Gson

case class MetricVisualization(resourceDisplayName:String){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}
object MetricVisualization {
  val DEFAULT = new MetricVisualization("mockResourceName")
}

trait MetricVisualizationGenerator {
  def generate(): MetricVisualization
}

object MetricVisualizationGenerator {

  def apply(s:String, resourceDetailsGenerator: Option[ResourceDetailsGenerator]=None): MetricVisualizationGenerator = s match {
    case "mock" => return new MetricVisualizationGeneratorMock
    case _ => return new MetricVisualizationGeneratorImpl(resourceDetailsGenerator.get.generate())
  }
  
  private class MetricVisualizationGeneratorImpl(resourceDetails: ResourceDetails) extends MetricVisualizationGenerator {
    override def generate(): MetricVisualization = {
      val metricVisualization = new MetricVisualization(resourceDetails.resourceName)
      metricVisualization
    }
  }

  private class MetricVisualizationGeneratorMock extends MetricVisualizationGenerator {
    override def generate(): MetricVisualization = MetricVisualization.DEFAULT
  }

}