package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.Lens
import com.grabtaxi.fincortex.dashboarding.utils.DateTimeFormats
import org.json4s.FieldSerializer.renameTo
import org.json4s.{FieldSerializer, Formats}

case class Lenses(lens:Lens)
object Lenses {
  val rename = FieldSerializer[Lenses](
    renameTo("lens", "0")
  )

  val formats: Formats = DateTimeFormats.format + rename
}
