package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.timeRange.TimeRangeValue
import com.grabtaxi.fincortex.dashboarding.utils.DateTimeFormats
import org.json4s.FieldSerializer.renameTo
import org.json4s.{FieldSerializer, Formats}

case class TimeRange(value:TimeRangeValue=new TimeRangeValue(), timeRangeType:String="MsPortalFx.Composition.Configuration.ValueTypes.TimeRange")
object TimeRange {
  val rename = FieldSerializer[TimeRange](
    renameTo("timeRangeType", "type")
  )

  val formats: Formats = DateTimeFormats.format + rename
}