package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart

import com.google.gson.Gson
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart.metrics.{MetricVisualization, MetricVisualizationGenerator, ResourceMetadata, ResourceMetadataGenerator}

case class ChartMetric(
  resourceMetadata: ResourceMetadata
  , metricVisualization: MetricVisualization
  , name : String
  , aggregationType: Int
){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}

object ChartMetric {
  val AGGREGATION_TYPE = Map (
    "sum" -> 1,
    "min" -> 2,
    "max" -> 3,
    "avg" -> 4
  )
}


trait ChartMetricGenerator {
  def generate():ChartMetric
}

object ChartMetricGenerator {
  def apply(s:String
            , resourceMetadataGenerator: Option[ResourceMetadataGenerator] = None
            , metricVisualizationGenerator: Option[MetricVisualizationGenerator] = None
            , name : Option[String] = Some("My Chart Metric")
            , aggregationType: Option[Int] = Some(ChartMetric.AGGREGATION_TYPE("avg"))
  ):ChartMetricGenerator = s match {
    case "mock" => return new ChartMetricGeneratorMock(name.get, aggregationType.get)
    case _ => return new ChartMetricGeneratorImpl(
      resourceMetadataGenerator.get.generate()
      , metricVisualizationGenerator.get.generate()
      , name.get
      , aggregationType.get
    )
  }

  private class ChartMetricGeneratorImpl(
      resourceMetadata: ResourceMetadata
     , metricVisualization: MetricVisualization
     , name : String
     , aggregationType: Int
  ) extends ChartMetricGenerator {
    override def generate(): ChartMetric = new ChartMetric(resourceMetadata, metricVisualization, name, aggregationType)
  }

  private class ChartMetricGeneratorMock(name:String, aggregationType:Int) extends  ChartMetricGenerator {
    val resourceMetadata = ResourceMetadataGenerator("mock").generate()
    val metricVisualization = MetricVisualizationGenerator("mock").generate()

    override def generate(): ChartMetric = new ChartMetric(resourceMetadata,metricVisualization,name,aggregationType)
  }
}