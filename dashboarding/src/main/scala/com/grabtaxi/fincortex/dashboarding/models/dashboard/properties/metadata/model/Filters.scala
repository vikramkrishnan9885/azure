package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.filters.FiltersValue

case class Filters(value:FiltersValue)
