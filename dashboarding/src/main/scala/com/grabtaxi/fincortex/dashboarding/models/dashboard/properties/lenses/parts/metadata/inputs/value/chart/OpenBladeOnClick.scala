package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart

import com.google.gson.Gson

case class OpenBladeOnClick(openBlade:Boolean){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}
object OpenBladeOnClick {
  val DEFAULT = new OpenBladeOnClick(true)
}



trait OpenBaldeOnClickGenerator {
  def generate(): OpenBladeOnClick
}

object OpenBaldeOnClickGenerator {
  def apply(s:String, openBlade:Option[Boolean]=None): OpenBaldeOnClickGenerator = s match {
    case "mock" => return new OpenBladeOnClickGeneratorMock
    case _ => return new OpenBaldeOnClickGeneratorImpl(openBlade.get)
  }

  private class OpenBaldeOnClickGeneratorImpl(openBlade:Boolean) extends OpenBaldeOnClickGenerator {
    override def generate(): OpenBladeOnClick = new OpenBladeOnClick(openBlade)
  }

  private class OpenBladeOnClickGeneratorMock extends OpenBaldeOnClickGenerator {
    override def generate(): OpenBladeOnClick = OpenBladeOnClick.DEFAULT
  }
}