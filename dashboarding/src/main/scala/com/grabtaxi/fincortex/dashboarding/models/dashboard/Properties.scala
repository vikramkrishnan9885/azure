package com.grabtaxi.fincortex.dashboarding.models.dashboard

import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.{Metadata, Lenses}

case class Properties(
  lenses:Lenses
  , metadata: Metadata
)
