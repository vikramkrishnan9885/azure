package com.grabtaxi.fincortex.dashboarding.utils

import java.text.SimpleDateFormat

import org.json4s.{DefaultFormats, Formats}
import org.json4s.ext.JodaTimeSerializers
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jackson.Serialization.write

import scala.util.Try

trait CustomJsonSerializer[T<:AnyRef] {

  def fromJson(jsonString: String)(formats: Formats)(implicit m: Manifest[T]): Option[T] = {
    Try(parse(jsonString).extract[T](formats,m)).toOption
  }

  def toJson(t: T)(formats: Formats):String = {
    write[T](t)(formats)
  }


}
