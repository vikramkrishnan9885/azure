package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.timeRange

case class RelativeTimeRangeValue(duration:Int=24, timeUnit:Int=1)
object RelativeTimeRangeValue {
  val default = new RelativeTimeRangeValue()
}
