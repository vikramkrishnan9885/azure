package com.grabtaxi.fincortex.dashboarding.utils

trait Generator[T] {
  def generate():T
}
