package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.metadata.model.timeRange

case class TimeRangeValue(relative:RelativeTimeRangeValue=RelativeTimeRangeValue.default)
object TimeRangeValue {
  val default = new TimeRangeValue()
}
