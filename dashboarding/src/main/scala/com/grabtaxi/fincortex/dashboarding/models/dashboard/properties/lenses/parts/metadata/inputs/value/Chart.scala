package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value

import com.google.gson.Gson
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.inputs.value.chart.{ChartMetric, ChartMetricGenerator, ChartVisualization, ChartVisualizationGenerator, OpenBaldeOnClickGenerator, OpenBladeOnClick}

case class Chart(
  metrics:Array[ChartMetric]
  , visualization:ChartVisualization
  , openBladeOnClick: OpenBladeOnClick
  , title:String = "Chart Title"
  , titleKind: Int = 2
){
  override def toString: String = {
    val gson = new Gson()
    val returnString = gson.toJson(this)
    returnString
  }
}



trait ChartGenerator {
  def generate():Chart
}
object ChartGenerator {
  def apply(
    s: String
    , metricGenerator: Option[ChartMetricGenerator] = None
    , visualizationGenerator: Option[ChartVisualizationGenerator] = None
    , openBaldeOnClickGenerator: Option[OpenBaldeOnClickGenerator] = None
    , title: Option[String] = None
    , titleKind: Option[Int] = None
   ):ChartGenerator = s match {
    case "mock" => return new ChartGeneratorMock
    case _ => return new ChartGeneratorImpl(
      metricGenerator.get
      , visualizationGenerator.get
      , openBaldeOnClickGenerator.get
      , title.get
      , titleKind.get
    )
  }

  private class ChartGeneratorMock extends ChartGenerator {
    override def generate(): Chart = new Chart(
      metrics = Array(ChartMetricGenerator("mock").generate())
      , visualization = ChartVisualization.DEFAULT
      , openBladeOnClick = OpenBladeOnClick.DEFAULT
    )
  }

  private class ChartGeneratorImpl(
    metricsGenerator:ChartMetricGenerator
    , visualizationGenerator: ChartVisualizationGenerator
    , openBaldeOnClickGenerator: OpenBaldeOnClickGenerator
    , title: String
    , titleKind: Int
  ) extends ChartGenerator {
    override def generate(): Chart = new Chart(
      Array(metricsGenerator.generate())
      , visualizationGenerator.generate()
      , openBaldeOnClickGenerator.generate()
      , title
      , titleKind
    )
  }
}