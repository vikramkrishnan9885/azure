package com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts

import com.google.gson.Gson
import com.grabtaxi.fincortex.dashboarding.models.dashboard.properties.lenses.parts.metadata.{LensPartMetadataInput, LensPartMetadataSettings}
import com.grabtaxi.fincortex.dashboarding.utils.DateTimeFormats
import org.json4s.FieldSerializer.renameTo
import org.json4s.{FieldSerializer, Formats}

case class LensPartMetadata(
 inputs:Array[Option[LensPartMetadataInput]]
 , settings: LensPartMetadataSettings=LensPartMetadataSettings.default
 , lensPartMetadataType:String="Extension[azure]/HubsExtension/PartType/MarkdownPart"
)
object LensPartMetadata {
  val rename = FieldSerializer[LensPartMetadata] (
    renameTo("lensPartMetadataType","type")
  )
  val formats:Formats= DateTimeFormats.format + rename

  val default = new LensPartMetadata(Array())
}