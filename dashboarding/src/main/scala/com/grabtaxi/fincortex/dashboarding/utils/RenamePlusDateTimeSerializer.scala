package com.grabtaxi.fincortex.dashboarding.utils

import java.text.SimpleDateFormat

import org.json4s.FieldSerializer.renameTo
import org.json4s.ext.JodaTimeSerializers
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jackson.Serialization.write
import org.json4s.{DefaultFormats, FieldSerializer}

import scala.util.Try

trait RenamePlusDateTimeSerializer[T<:AnyRef] {
  val name: String
  val newName: String

  def rename(implicit m: Manifest[T]) = FieldSerializer[T](
    renameTo(name, newName)
  )

  def formats(implicit m: Manifest[T]) = new DefaultFormats {
      override def dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
  } ++ JodaTimeSerializers.all + rename


  def fromJson(jsonString: String)(implicit m:Manifest[T]): Option[T] = {
    Try(parse(jsonString).extract[T](formats,m)).toOption
  }

  def toJson(t: T)(implicit m: Manifest[T]):String = {
    write[T](t)(formats)
  }

}
