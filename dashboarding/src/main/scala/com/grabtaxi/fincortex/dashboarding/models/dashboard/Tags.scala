package com.grabtaxi.fincortex.dashboarding.models.dashboard

import com.grabtaxi.fincortex.dashboarding.utils.{CustomJsonSerializer, DateTimeFormats}
import org.json4s.FieldSerializer.renameTo
import org.json4s.{FieldSerializer, Formats}

case class Tags (hiddenTitle:String)
object Tags {
  val rename = FieldSerializer[Tags](
    renameTo("hiddenTitle", "hidden-title")
  )

  val formats: Formats = DateTimeFormats.format + rename
}
