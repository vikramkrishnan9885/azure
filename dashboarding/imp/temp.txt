


trait TagsJsonSerializer extends CustomJsonSerializer[Tags]{
  val tagFormats = Tags.formats

  def fromJson(jsonString: String)
    (implicit m: Manifest[Tags])
  : Option[Tags] = super.fromJson(jsonString)(tagFormats)

  def toJson(t: Tags): String = super.toJson(t)(tagFormats)
}
object TagsJsonSerializer extends TagsJsonSerializer


