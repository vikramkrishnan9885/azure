resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "West Europe"
}

resource "random_id" "storage_account" {
  byte_length = 8
}

// AZURE BLOB STORE
// Randomize name
resource "azurerm_storage_account" "example" {
  //name                     = "examplestoracc"
  name                     = "tfsta${lower(random_id.storage_account.hex)}"
  resource_group_name      = "${azurerm_resource_group.example.name}"
  location                 = "${azurerm_resource_group.example.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "example" {
  name                  = "content"
  resource_group_name   = "${azurerm_resource_group.example.name}"
  storage_account_name  = "${azurerm_storage_account.example.name}"
  container_access_type = "private"
}

// EVENTHUB
// Randomize name
resource "azurerm_eventhub_namespace" "example" {
  //name                = "acceptanceTestEventHubNamespace"
  name                = "acceptanceTestEventHubNamespace${lower(random_id.storage_account.hex)}"
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"
  sku                 = "Standard"
  capacity            = 1

  tags = {
    environment = "Production"
  }
}

resource "azurerm_eventhub" "example" {
  name                = "acceptanceTestEventHub"
  namespace_name      = "${azurerm_eventhub_namespace.example.name}"
  resource_group_name = "${azurerm_resource_group.example.name}"
  partition_count     = 2
  message_retention   = 1
}

resource "azurerm_eventhub_namespace_authorization_rule" "example" {
  name                = "navi"
  namespace_name      = "${azurerm_eventhub_namespace.example.name}"
  resource_group_name = "${azurerm_resource_group.example.name}"

  listen = true
  send   = true
  manage = true
}

// LOG ANALYTICS WORKSPACE
// Randomize name
resource "azurerm_log_analytics_workspace" "example" {
  //name                = "acctest-01"
  name                = "acctest-01${lower(random_id.storage_account.hex)}"
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"
  sku                 = "PerGB2018"
  retention_in_days   = 30
}

// THIS WILL NEED TO BE EDITED
resource "azurerm_log_analytics_solution" "example" {
  solution_name         = "ContainerInsights"
  location              = "${azurerm_resource_group.example.location}"
  resource_group_name   = "${azurerm_resource_group.example.name}"
  workspace_resource_id = "${azurerm_log_analytics_workspace.example.id}"
  workspace_name        = "${azurerm_log_analytics_workspace.example.name}"

  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/ContainerInsights"
  }
}

// DIAGNOSTIC SETTING
// NEED TO CHANGE THIS
// Create resource
//data "azurerm_key_vault" "example" {
//  name                = "example-vault"
//  resource_group_name = "${azurerm_resource_group.example.name}"
//}

resource "azurerm_data_factory" "example" {
  name                = "example${lower(random_id.storage_account.hex)}"
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"
}

// https://www.terraform.io/docs/providers/azurerm/r/monitor_diagnostic_setting.html
resource "azurerm_monitor_diagnostic_setting" "example" {
  name               = "example"
  target_resource_id = "${azurerm_data_factory.example.id}"
  storage_account_id = "${azurerm_storage_account.example.id}"

  log {
    category = "PipelineRuns"
    enabled  = false

    retention_policy {
      enabled = false
    }
  }

  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = false
    }
  }
}