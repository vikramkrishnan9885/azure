resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "West Europe"
}

resource "random_id" "storage_account" {
  byte_length = 8
}

// AZURE BLOB STORE
// Randomize name
resource "azurerm_storage_account" "example" {
  //name                     = "examplestoracc"
  name                     = "tfsta${lower(random_id.storage_account.hex)}"
  resource_group_name      = "${azurerm_resource_group.example.name}"
  location                 = "${azurerm_resource_group.example.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "example" {
  name                  = "content"
  resource_group_name   = "${azurerm_resource_group.example.name}"
  storage_account_name  = "${azurerm_storage_account.example.name}"
  container_access_type = "private"
}

// EVENTHUB
// Randomize name
resource "azurerm_eventhub_namespace" "example" {
  //name                = "acceptanceTestEventHubNamespace"
  name                = "acceptanceTestEventHubNamespace${lower(random_id.storage_account.hex)}"
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"
  sku                 = "Standard"
  capacity            = 1

  tags = {
    environment = "Production"
  }
}

resource "azurerm_eventhub" "example" {
  name                = "acceptanceTestEventHub"
  namespace_name      = "${azurerm_eventhub_namespace.example.name}"
  resource_group_name = "${azurerm_resource_group.example.name}"
  partition_count     = 2
  message_retention   = 1
}

resource "azurerm_eventhub_namespace_authorization_rule" "example" {
  name                = "navi"
  namespace_name      = "${azurerm_eventhub_namespace.example.name}"
  resource_group_name = "${azurerm_resource_group.example.name}"

  listen = true
  send   = true
  manage = true
}

// LOG ANALYTICS WORKSPACE
// Randomize name
resource "azurerm_log_analytics_workspace" "example" {
  //name                = "acctest-01"
  name                = "acctest-01${lower(random_id.storage_account.hex)}"
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"
  sku                 = "PerGB2018"
  retention_in_days   = 30
}

// THIS WILL NEED TO BE EDITED
resource "azurerm_log_analytics_solution" "example" {
  solution_name         = "ContainerInsights"
  location              = "${azurerm_resource_group.example.location}"
  resource_group_name   = "${azurerm_resource_group.example.name}"
  workspace_resource_id = "${azurerm_log_analytics_workspace.example.id}"
  workspace_name        = "${azurerm_log_analytics_workspace.example.name}"

  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/ContainerInsights"
  }
}

// SOLUTIONS TO ENABLE
resource "azurerm_log_analytics_solution" "example-Security" {
  solution_name         = "Security"
  location              = "${azurerm_resource_group.example.location}"
  resource_group_name   = "${azurerm_resource_group.example.name}"
  workspace_resource_id = "${azurerm_log_analytics_workspace.example.id}"
  workspace_name        = "${azurerm_log_analytics_workspace.example.name}"

  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/Security"
  }
}

resource "azurerm_log_analytics_solution" "example-Updates" {
  solution_name         = "Updates"
  location              = "${azurerm_resource_group.example.location}"
  resource_group_name   = "${azurerm_resource_group.example.name}"
  workspace_resource_id = "${azurerm_log_analytics_workspace.example.id}"
  workspace_name        = "${azurerm_log_analytics_workspace.example.name}"

  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/Updates"
  }
}

resource "azurerm_log_analytics_solution" "example-SQLAssessment" {
  solution_name         = "SQLAssessment"
  location              = "${azurerm_resource_group.example.location}"
  resource_group_name   = "${azurerm_resource_group.example.name}"
  workspace_resource_id = "${azurerm_log_analytics_workspace.example.id}"
  workspace_name        = "${azurerm_log_analytics_workspace.example.name}"

  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/SQLAssessment"
  }
}

// DIAGNOSTIC SETTING
// NEED TO CHANGE THIS
// Create resource
//data "azurerm_key_vault" "example" {
//  name                = "example-vault"
//  resource_group_name = "${azurerm_resource_group.example.name}"
//}

resource "azurerm_data_factory" "example" {
  name                = "example${lower(random_id.storage_account.hex)}"
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"
}

// https://www.terraform.io/docs/providers/azurerm/r/monitor_diagnostic_setting.html
resource "azurerm_monitor_diagnostic_setting" "example" {
  name               = "example"
  target_resource_id = "${azurerm_data_factory.example.id}"
  storage_account_id = "${azurerm_storage_account.example.id}"

  log {
    category = "PipelineRuns"
    enabled  = false

    retention_policy {
      enabled = false
    }
  }

  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = false
    }
  }
}

/////////////////////////////////////////////////////////////
//
// ALL THIS JAZZ IS TO CREATE THE VM
//
///////////////////////////////////////////////////////////////

variable "prefix" {
  default = "tfvmex"
}

resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = "${azurerm_resource_group.example.name}"
  virtual_network_name = "${azurerm_virtual_network.main.name}"
  address_prefix       = "10.0.2.0/24"
}

resource "azurerm_network_interface" "main" {
  name                = "${var.prefix}-nic"
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = "${azurerm_subnet.internal.id}"
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_virtual_machine" "main" {
    name                  = "myVM"
    location              = "${azurerm_resource_group.example.location}"
    resource_group_name   = "${azurerm_resource_group.example.name}"
    network_interface_ids = ["${azurerm_network_interface.main.id}"]
    vm_size               = "Standard_DS1_v2"

    # Uncomment this line to delete the OS disk automatically when deleting the VM
    delete_os_disk_on_termination = true


    # Uncomment this line to delete the data disks automatically when deleting the VM
    delete_data_disks_on_termination = true

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04-LTS"
        version   = "latest"
    }
    storage_os_disk {
        name              = "myosdisk1"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
    }
    os_profile {
        computer_name  = "hostname"
        admin_username = "testadmin"
        admin_password = "Password1234!"
    }
    os_profile_linux_config {
        disable_password_authentication = false
    }
    tags = {
        environment = "staging"
    }
}

//////////////////////////////////////////////////////////////////

resource "azurerm_template_deployment" "example01" {
  name                = "acctesttemplate-01"
  resource_group_name = "${azurerm_resource_group.example.name}"

  template_body = <<DEPLOY
{
  "$schema": "https://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
  "contentVersion": "1.0.0.0",
  "parameters": {
    "actionGroupName": {
      "type": "string",
      "metadata": {
        "description": "Unique name (within the Resource Group) for the Action group."
      }
    },
    "actionGroupShortName": {
      "type": "string",
      "metadata": {
        "description": "Short name (maximum 12 characters) for the Action group."
      }
    }
  },
  "resources": [
    {
      "type": "Microsoft.Insights/actionGroups",
      "apiVersion": "2018-03-01",
      "name": "[parameters('actionGroupName')]",
      "location": "Global",
      "properties": {
        "groupShortName": "[parameters('actionGroupShortName')]",
        "enabled": true,
        "smsReceivers": [
          {
            "name": "contosoSMS",
            "countryCode": "1",
            "phoneNumber": "5555551212"
          },
          {
            "name": "contosoSMS2",
            "countryCode": "1",
            "phoneNumber": "5555552121"
          }
        ],
        "emailReceivers": [
          {
            "name": "contosoEmail",
            "emailAddress": "devops@contoso.com"
          },
          {
            "name": "contosoEmail2",
            "emailAddress": "devops2@contoso.com"
          }
        ],
        "webhookReceivers": [
          {
            "name": "contosoHook",
            "serviceUri": "http://requestb.in/1bq62iu1"
          },
          {
            "name": "contosoHook2",
            "serviceUri": "http://requestb.in/1bq62iu2"
          }
        ]
      }
    }
  ],
  "outputs":{
      "actionGroupId":{
          "type":"string",
          "value":"[resourceId('Microsoft.Insights/actionGroups',parameters('actionGroupName'))]"
      }
  }
}
DEPLOY

  # these key-value pairs are passed into the ARM Template's `parameters` block
  parameters = {
    "actionGroupName" = "MyActionGroup"
    "actionGroupShortName" = "xyz"
  }

  deployment_mode = "Incremental"
}

output "actionGroupId" {
  value = "${lookup(azurerm_template_deployment.example01.outputs, "actionGroupId")}"
}

// BUGGER
// https://stackoverflow.com/questions/49534334/q-can-i-insert-integer-number-in-parameter-code-for-azures-json-template-code
resource "azurerm_template_deployment" "main" {
    name                = "MyApp-ARM"
    resource_group_name = "${azurerm_resource_group.example.name}"
    
    template_body = "${file("arm/azuredeploy.json")}"
    
    parameters = {
        "alertName" = "New Metric Alert"
        "alertDescription"= "New metric alert created via template"
        "alertSeverity"="3"
        "isEnabled"="true"
        "resourceId"="/subscriptions/a4a8c613-6bb3-4a55-90cd-be8a39ef320e/resourceGroups/example-resources/providers/Microsoft.Compute/virtualMachines/myVM"        
        "metricName"= "Percentage CPU"
        "operator"="GreaterThan"
        "threshold"="80"
        "timeAggregation"="Average"
        "actionGroupId"= "/subscriptions/a4a8c613-6bb3-4a55-90cd-be8a39ef320e/resourceGroups/example-resources/providers/Microsoft.Insights/actionGroups/MyActionGroup"
    }
    
    deployment_mode = "Incremental"
}

variable "md_content" {
  description = "Content for the MD tile"
  default     = "# Hello all :)"
}

variable "video_link" {
  description = "Link to a video"
  default     = "https://www.youtube.com/watch?v=......"
}

data "azurerm_subscription" "current" {}

resource "azurerm_dashboard" "my-board" {
  name                = "my-cool-dashboard"
  resource_group_name = "${azurerm_resource_group.example.name}"
  location            = "${azurerm_resource_group.example.location}"
  tags = {
    source = "terraform"
  }
  dashboard_properties = <<DASH
{
   "lenses": {
        "0": {
            "order": 0,
            "parts": {
                "0": {
                    "position": {
                        "x": 0,
                        "y": 0,
                        "rowSpan": 2,
                        "colSpan": 3
                    },
                    "metadata": {
                        "inputs": [],
                        "type": "Extension/HubsExtension/PartType/MarkdownPart",
                        "settings": {
                            "content": {
                                "settings": {
                                    "content": "${var.md_content}",
                                    "subtitle": "",
                                    "title": ""
                                }
                            }
                        }
                    }
                },               
                "1": {
                    "position": {
                        "x": 5,
                        "y": 0,
                        "rowSpan": 4,
                        "colSpan": 6
                    },
                    "metadata": {
                        "inputs": [],
                        "type": "Extension/HubsExtension/PartType/VideoPart",
                        "settings": {
                            "content": {
                                "settings": {
                                    "title": "Important Information",
                                    "subtitle": "",
                                    "src": "${var.video_link}",
                                    "autoplay": true
                                }
                            }
                        }
                    }
                },
                "2": {
                    "position": {
                        "x": 0,
                        "y": 4,
                        "rowSpan": 4,
                        "colSpan": 6
                    },
                    "metadata": {
                        "inputs": [
                            {
                                "name": "ComponentId",
                                "value": "/subscriptions/${data.azurerm_subscription.current.subscription_id}/resourceGroups/myRG/providers/microsoft.insights/components/myWebApp"
                            }
                        ],
                        "type": "Extension/AppInsightsExtension/PartType/AppMapGalPt",
                        "settings": {},
                        "asset": {
                            "idInputName": "ComponentId",
                            "type": "ApplicationInsights"
                        }
                    }
                }              
            }
        }
    },
    "metadata": {
        "model": {
            "timeRange": {
                "value": {
                    "relative": {
                        "duration": 24,
                        "timeUnit": 1
                    }
                },
                "type": "MsPortalFx.Composition.Configuration.ValueTypes.TimeRange"
            },
            "filterLocale": {
                "value": "en-us"
            },
            "filters": {
                "value": {
                    "MsPortalFx_TimeRange": {
                        "model": {
                            "format": "utc",
                            "granularity": "auto",
                            "relative": "24h"
                        },
                        "displayCache": {
                            "name": "UTC Time",
                            "value": "Past 24 hours"
                        },
                        "filteredPartIds": [
                            "StartboardPart-UnboundPart-ae44fef5-76b8-46b0-86f0-2b3f47bad1c7"
                        ]
                    }
                }
            }
        }
    }
}
DASH
}

////////////////////////////////////////////////////
//
//  SAVED SEARCHES
//
/////////////////////////////////////////////////////

//resource "azurerm_template_deployment" "searches" {
//    name                = "searches"
//    resource_group_name = "${azurerm_resource_group.example.name}"
//    
//    template_body = "${file("arm/exported-searches.json")}"
//    
#     parameters = {
#         "workspaceName" = "${azurerm_log_analytics_workspace.example.name}"
#         "applicationDiagnosticsStorageAccountName"="${azurerm_storage_account.example.name}"
#         "applicationDiagnosticsStorageAccountResourceGroup"="${azurerm_resource_group.example.name}"
#     }  
#     deployment_mode = "Incremental"
# }

resource "azurerm_template_deployment" "searches" {
    name                = "searches"
    resource_group_name = "${azurerm_resource_group.example.name}"
    
    template_body = "${file("arm/saved-searches.json")}"

    parameters = {
      "workspacename"  = "${azurerm_log_analytics_workspace.example.name}"
      //"accountName"  = "myAccount"
      "workspaceregionId"  = "West Europe"
      //"regionId"  = "West Europe"
      //"pricingTier"  = "Free"
      "actiongroup"  = "/subscriptions/a4a8c613-6bb3-4a55-90cd-be8a39ef320e/resourceGroups/example-resources/providers/Microsoft.Insights/actionGroups/MyActionGroup"

    }

    deployment_mode = "Incremental"
}