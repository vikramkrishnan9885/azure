ITSG003877-MAC:~ vikram.krishnan$ pwsh
PowerShell 6.2.3
Copyright (c) Microsoft Corporation. All rights reserved.

https://aka.ms/pscore6-docs
Type 'help' to get help.

PS /Users/vikram.krishnan> Import-Module Az
WARNING: Both Az and AzureRM modules were detected on this machine. Az and AzureRM modules cannot be imported in the same session or used in the same script or runbook. If you are running PowerShell in an environment you control you can use the 'Uninstall-AzureRm' cmdlet to remove all AzureRm modules from your machine. If you are running in Azure Automation, take care that none of your runbooks import both Az and AzureRM modules. More information can be found here: https://aka.ms/azps-migration-guide
PS /Users/vikram.krishnan> Connect-AzAccount
WARNING: To sign in, use a web browser to open the page https://microsoft.com/devicelogin and enter the code HRNSE2QCD to authenticate.

Account               SubscriptionName TenantId                             Env
                                                                            iro
                                                                            nme
                                                                            nt
-------               ---------------- --------                             ---
kvikram9885@gmail.com Free Trial       373de13a-dcae-4c03-ad1c-371f679f8009 Az…

PS /Users/vikram.krishnan> New-AzResourceGroup -Name TutorialResources -Location eastus

ResourceGroupName : TutorialResources
Location          : eastus
ProvisioningState : Succeeded
Tags              : 
ResourceId        : /subscriptions/a4a8c613-6bb3-4a55-90cd-be8a39ef320e/resourc
                    eGroups/TutorialResources


PS /Users/vikram.krishnan> $cred = Get-Credential -Message "Enter a username and password for the virtual machine."

PowerShell credential request
Enter a username and password for the virtual machine.
User: tutorAdmin
Password for user tutorAdmin: **********

PS /Users/vikram.krishnan> Get-Command -Verb Get -Noun AzVM* -Module Az.Compute

CommandType     Name                                               Version    S
                                                                              o
                                                                              u
                                                                              r
                                                                              c
                                                                              e
-----------     ----                                               -------    -
Alias           Get-AzVmssDiskEncryptionStatus                     3.0.0      A
Alias           Get-AzVmssVMDiskEncryptionStatus                   3.0.0      A
Cmdlet          Get-AzVM                                           3.0.0      A
Cmdlet          Get-AzVMAccessExtension                            3.0.0      A
Cmdlet          Get-AzVMADDomainExtension                          3.0.0      A
Cmdlet          Get-AzVMAEMExtension                               3.0.0      A
Cmdlet          Get-AzVMBootDiagnosticsData                        3.0.0      A
Cmdlet          Get-AzVMChefExtension                              3.0.0      A
Cmdlet          Get-AzVMCustomScriptExtension                      3.0.0      A
Cmdlet          Get-AzVMDiagnosticsExtension                       3.0.0      A
Cmdlet          Get-AzVMDiskEncryptionStatus                       3.0.0      A
Cmdlet          Get-AzVMDscExtension                               3.0.0      A
Cmdlet          Get-AzVMDscExtensionStatus                         3.0.0      A
Cmdlet          Get-AzVMExtension                                  3.0.0      A
Cmdlet          Get-AzVMExtensionImage                             3.0.0      A
Cmdlet          Get-AzVMExtensionImageType                         3.0.0      A
Cmdlet          Get-AzVMImage                                      3.0.0      A
Cmdlet          Get-AzVMImageOffer                                 3.0.0      A
Cmdlet          Get-AzVMImagePublisher                             3.0.0      A
Cmdlet          Get-AzVMImageSku                                   3.0.0      A
Cmdlet          Get-AzVMRunCommandDocument                         3.0.0      A
Cmdlet          Get-AzVMSize                                       3.0.0      A
Cmdlet          Get-AzVMSqlServerExtension                         3.0.0      A
Cmdlet          Get-AzVmss                                         3.0.0      A
Cmdlet          Get-AzVmssDiskEncryption                           3.0.0      A
Cmdlet          Get-AzVmssRollingUpgrade                           3.0.0      A
Cmdlet          Get-AzVmssSku                                      3.0.0      A
Cmdlet          Get-AzVmssVM                                       3.0.0      A
Cmdlet          Get-AzVmssVMDiskEncryption                         3.0.0      A
Cmdlet          Get-AzVMUsage                                      3.0.0      A

PS /Users/vikram.krishnan> $vmParams = @{
>>   ResourceGroupName = 'TutorialResources'
>>   Name = 'TutorialVM1'
>>   Location = 'eastus'
>>   ImageName = 'Win2016Datacenter'
>>   PublicIpAddressName = 'tutorialPublicIp'
>>   Credential = $cred
>>   OpenPorts = 3389
>> }
PS /Users/vikram.krishnan> $newVM1 = New-AzVM @vmParams
New-AzVM : The supplied password must be between 8-123 characters long and must satisfy at least 3 of password complexity requirements from the following:      1) Contains an uppercase character                                              2) Contains a lowercase character                                               3) Contains a numeric digit                                                     4) Contains a special character                                                 5) Control characters are not allowed                                           At line:1 char:11                                                               + $newVM1 = New-AzVM @vmParams                                                  +           ~~~~~~~~~~~~~~~~~~                                                  + CategoryInfo          : CloseError: (:) [New-AzVM], CloudException            + FullyQualifiedErrorId : Microsoft.Azure.Commands.Compute.NewAzureVMCommand                                                                                    PS /Users/vikram.krishnan> $cred = Get-Credential -Message "Enter a username and password for the virtual machine."                                                                                                                             PowerShell credential request                                                   Enter a username and password for the virtual machine.                          User: tutorAdmin                                                                Password for user tutorAdmin: *********                                         
                     
PS /Users/vikram.krishnan> $vmParams = @{                                       >>   ResourceGroupName = 'TutorialResources'
>>   Name = 'TutorialVM1'
>>   Location = 'eastus'
>>   ImageName = 'Win2016Datacenter'
>>   PublicIpAddressName = 'tutorialPublicIp'
>>   Credential = $cred
>>   OpenPorts = 3389
>> }
PS /Users/vikram.krishnan> $newVM1 = New-AzVM @vmParams                         PS /Users/vikram.krishnan> $newVM1                                                                                                                                                                                                              ResourceGroupName        : TutorialResources                                    Id                       : /subscriptions/a4a8c613-6bb3-4a55-90cd-be8a39ef320e/ resourceGroups/TutorialResources/providers/Microsoft.Compute/virtualMachines/Tu torialVM1                                                                       
VmId                     : 15a841af-ec03-45a4-969b-5b175a8eea24
Name                     : TutorialVM1
Type                     : Microsoft.Compute/virtualMachines
Location                 : eastus
Tags                     : {}
HardwareProfile          : {VmSize}
NetworkProfile           : {NetworkInterfaces}
OSProfile                : {ComputerName, AdminUsername, WindowsConfiguration, 
Secrets, AllowExtensionOperations, RequireGuestProvisionSignal}
ProvisioningState        : Succeeded
StorageProfile           : {ImageReference, OsDisk, DataDisks}
FullyQualifiedDomainName : tutorialvm1-8e2186.eastus.cloudapp.azure.com


PS /Users/vikram.krishnan> $newVM1.OSProfile | Select-Object ComputerName,AdminUserName

ComputerName AdminUsername
------------ -------------
TutorialVM1  tutorAdmin

PS /Users/vikram.krishnan> $newVM1 | Get-AzNetworkInterface |
>>   Select-Object -ExpandProperty IpConfigurations |
>>     Select-Object Name,PrivateIpAddress

Name        PrivateIpAddress
----        ----------------
TutorialVM1 192.168.1.4

PS /Users/vikram.krishnan> $publicIp = Get-AzPublicIpAddress -Name tutorialPublicIp -ResourceGroupName TutorialResources
PS /Users/vikram.krishnan> 
PS /Users/vikram.krishnan> $publicIp | Select-Object Name,IpAddress,@{label='FQDN';expression={$_.DnsSettings.Fqdn}}

Name             IpAddress    FQDN
----             ---------    ----
tutorialPublicIp 13.68.200.74 tutorialvm1-54578c.eastus.cloudapp.azure.com

PS /Users/vikram.krishnan> mstsc.exe /v <PUBLIC_IP_ADDRESS>
At line:1 char:14
+ mstsc.exe /v <PUBLIC_IP_ADDRESS>
+              ~
The '<' operator is reserved for future use.
+ CategoryInfo          : ParserError: (:) [], ParentContainsErrorRecordException
+ FullyQualifiedErrorId : RedirectionNotSupported
 
PS /Users/vikram.krishnan> mstsc.exe /v 13.68.200.74       
mstsc.exe : The term 'mstsc.exe' is not recognized as the name of a cmdlet, function, script file, or operable program.
Check the spelling of the name, or if a path was included, verify that the path is correct and try again.
At line:1 char:1
+ mstsc.exe /v 13.68.200.74
+ ~~~~~~~~~
+ CategoryInfo          : ObjectNotFound: (mstsc.exe:String) [], CommandNotFoundException
+ FullyQualifiedErrorId : CommandNotFoundException
 
PS /Users/vikram.krishnan> open rdp://13.68.200.74  
The file /Users/vikram.krishnan/rdp:/13.68.200.74 does not exist.
PS /Users/vikram.krishnan> # Creating a new VM on the existing subnet
PS /Users/vikram.krishnan> $vm2Params = @{
>>   ResourceGroupName = 'TutorialResources'
>>   Name = 'TutorialVM2'
>>   ImageName = 'Win2016Datacenter'
>>   VirtualNetworkName = 'TutorialVM1'
>>   SubnetName = 'TutorialVM1'
>>   PublicIpAddressName = 'tutorialPublicIp2'
>>   Credential = $cred
>>   OpenPorts = 3389
>> }
PS /Users/vikram.krishnan> $newVM2 = New-AzVM @vm2Params
PS /Users/vikram.krishnan>                                                      PS /Users/vikram.krishnan> $newVM2                                                                                                                                                                                                              ResourceGroupName        : TutorialResources                                    Id                       : /subscriptions/a4a8c613-6bb3-4a55-90cd-be8a39ef320e/ resourceGroups/TutorialResources/providers/Microsoft.Compute/virtualMachines/Tu torialVM2                                                                       
VmId                     : 12daefe8-291b-4527-ae67-bf5fe5e8e773
Name                     : TutorialVM2
Type                     : Microsoft.Compute/virtualMachines
Location                 : eastus
Tags                     : {}
HardwareProfile          : {VmSize}
NetworkProfile           : {NetworkInterfaces}
OSProfile                : {ComputerName, AdminUsername, WindowsConfiguration, 
Secrets, AllowExtensionOperations, RequireGuestProvisionSignal}
ProvisioningState        : Succeeded
StorageProfile           : {ImageReference, OsDisk, DataDisks}
FullyQualifiedDomainName : tutorialvm2-94c696.eastus.cloudapp.azure.com


PS /Users/vikram.krishnan> bash /Users/vikram.krishnan/Desktop/azure/run.sh
Hello, world!
PS /Users/vikram.krishnan> open rdp://$newVM2.FullyQualifiedDomainName
The file /Users/vikram.krishnan/rdp:/Microsoft.Azure.Commands.Compute.Models.PSVirtualMachine.FullyQualifiedDomainName does not exist.
PS /Users/vikram.krishnan> $job = Remove-AzResourceGroup -Name TutorialResources -Force -AsJob
PS /Users/vikram.krishnan> 
PS /Users/vikram.krishnan> $job

Id     Name            PSJobTypeName   State         HasMoreData     Location
--     ----            -------------   -----         -----------     --------  
1      Long Running O… AzureLongRunni… Running       True            localhost 

PS /Users/vikram.krishnan> # The -AsJob parameter keeps PowerShell from blocking while the deletion takes place.
PS /Users/vikram.krishnan> # The -AsJob parameter keeps PowerShell from blocking while the deletion takes place.
PS /Users/vikram.krishnan> $job                                                                                 
Id     Name            PSJobTypeName   State         HasMoreData     Location
--     ----            -------------   -----         -----------     --------  
1      Long Running O… AzureLongRunni… Running       True            localhost 

PS /Users/vikram.krishnan> $job

Id     Name            PSJobTypeName   State         HasMoreData     Location
--     ----            -------------   -----         -----------     --------  
1      Long Running O… AzureLongRunni… Running       True            localhost 

PS /Users/vikram.krishnan> $job

Id     Name            PSJobTypeName   State         HasMoreData     Location
--     ----            -------------   -----         -----------     --------  
1      Long Running O… AzureLongRunni… Running       True            localhost 

PS /Users/vikram.krishnan> $job

Id     Name            PSJobTypeName   State         HasMoreData     Location
--     ----            -------------   -----         -----------     --------  
1      Long Running O… AzureLongRunni… Running       True            localhost 

PS /Users/vikram.krishnan> $job

Id     Name            PSJobTypeName   State         HasMoreData     Location             Command
--     ----            -------------   -----         -----------     --------             -------
1      Long Running O… AzureLongRunni… Running       True            localhost            Remove-AzRe…

PS /Users/vikram.krishnan> $job

Id     Name            PSJobTypeName   State         HasMoreData     Location             Command
--     ----            -------------   -----         -----------     --------             -------
1      Long Running O… AzureLongRunni… Running       True            localhost            Remove-AzRe…

PS /Users/vikram.krishnan> $job

Id     Name            PSJobTypeName   State         HasMoreData     Location             Command
--     ----            -------------   -----         -----------     --------             -------
1      Long Running O… AzureLongRunni… Running       True            localhost            Remove-AzRe…

PS /Users/vikram.krishnan> 
PS /Users/vikram.krishnan> $job                                                                                 
Id     Name            PSJobTypeName   State         HasMoreData     Location             Command
--     ----            -------------   -----         -----------     --------             -------
1      Long Running O… AzureLongRunni… Running       True            localhost            Remove-AzRe…

PS /Users/vikram.krishnan> 
